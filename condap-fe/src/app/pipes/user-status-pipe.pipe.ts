import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/user.model';

@Pipe({
  name: 'userStatusPipe',
  pure: false
})
export class UserStatusPipePipe implements PipeTransform {

  transform(users: User[], status: boolean): any {
    if(!users) return users;
    return users.filter(user => user.active == status);
  }

}
