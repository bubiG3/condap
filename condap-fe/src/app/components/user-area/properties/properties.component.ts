import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Payment } from 'src/app/models/payment.model';
import { PropertyType } from 'src/app/models/property-type.model';
import { Property } from 'src/app/models/property.model';
import { User } from 'src/app/models/user.model';
import { PaymentService } from 'src/app/services/payment.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { ReceiptService } from 'src/app/services/receipt.service';
import { UsersService } from 'src/app/services/users.service';
import { ViewEditModalComponent } from '../users/view-edit-modal/view-edit-modal.component';
import { PropertyModalComponent } from './property-modal/property-modal.component';

declare var require: any
const FileSaver = require('file-saver');

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit {

  page: number = 1;

  @Input() currentUser: User;

  properties: Property[];
  propertyTypes: PropertyType[];
  asc: Boolean = null;
  sortBy: string = null;

  constructor(public dialog: MatDialog, 
    private propertyService: PropertiesService,
    private userService: UsersService,
    private paymentService: PaymentService,
    private receiptService: ReceiptService,
    public datePipe: DatePipe) { }

  ngOnInit(): void {
    this.propertyService.propertyList.subscribe(list => this.properties = list);
  }

  openPropertyModal(action: string, property: Property){
    this.dialog.open(PropertyModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        action: action,
        property: property,
        currentUser: this.currentUser
      }
    })
  }

  openUser(userId: number){
    this.dialog.open(ViewEditModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        user: this.userService.selectUserFromList(userId),
        currentUser: this.currentUser
      }
    });
  }

  downloadReceipt(property: Property){
    var payment: Payment = this.paymentService.getPayments.find(x => x.property.id == property.id);
    this.receiptService.downloadReceipt(payment.id).subscribe(data => {
      const fileName = payment.property.parcel_number + payment.property.block + "-" +  
        this.datePipe.transform(payment.start_date, 'dd.MM.yyyy') + "-" + 
        this.datePipe.transform(payment.end_date, 'dd.MM.yyyy') + ".pdf";
      FileSaver.saveAs(data.images, fileName);
    })
  }

  get IsFetching(){
    return this.propertyService.fetching;
  }

  sort(param: string){
    this.sortBy = param;
    if(this.asc == null){
      this.asc = false;
    }
    if(param == "type"){
      if(this.asc){
        this.properties.sort((a,b) => b.type.name.localeCompare(a.type.name));
        this.asc = false;
      }
      else{
        this.properties.sort((a,b) => a.type.name.localeCompare(b.type.name));
        this.asc = true;
      }
    }
    if(param == "landlord"){
      if(this.asc){
        this.properties.sort((a,b) => {
          return (b.landlord[0]?.first_name ? b.landlord[0].first_name : "zzz")
            .localeCompare(a.landlord[0]?.first_name ? a.landlord[0].first_name : "zzz")
        });
        this.asc = false;
      }
      else{
        this.properties.sort((a,b) => {
          return (a.landlord[0]?.first_name ? a.landlord[0].first_name : "zzz")
            .localeCompare(b.landlord[0]?.first_name ? b.landlord[0].first_name : "zzz")
        });
        this.asc = true;
      }
    }
    if(param == "id"){
      if(this.asc){
        this.properties.sort((a,b) => b.parcel_number - a.parcel_number);
        this.asc = false;
      }
      else{
        this.properties.sort((a,b) => a.parcel_number - b.parcel_number);
        this.asc = true;
      }
    }
    if(param == "status"){
      if(this.asc){
        this.properties.sort((a,b) => b.payment_status.localeCompare(a.payment_status));
        this.asc = false;
      }
      else{
        this.properties.sort((a,b) => a.payment_status.localeCompare(b.payment_status));
        this.asc = true;
      }
    }
  }
}
