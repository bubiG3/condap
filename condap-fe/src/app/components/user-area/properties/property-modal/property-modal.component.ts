import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { PropertyDTO } from 'src/app/models/DTOs/property-dto.model';
import { PropertyType } from 'src/app/models/property-type.model';
import { Property } from 'src/app/models/property.model';
import { User } from 'src/app/models/user.model';
import { PropertiesService } from 'src/app/services/properties.service';
import { PropertyTypeService } from 'src/app/services/property-type.service';
import { UsersService } from 'src/app/services/users.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { ViewEditModalComponent } from '../../users/view-edit-modal/view-edit-modal.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-property-modal',
  templateUrl: './property-modal.component.html',
  styleUrls: ['./property-modal.component.scss']
})
export class PropertyModalComponent implements OnInit {

  editing: boolean = false;
  property: Property;
  currentUser: User;
  propertyForm: FormGroup;

  users: User[];
  propertyTypes: PropertyType[];

  landlordOptions: string[];
  filteredLandlords: Observable<string[]>;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
    public dialog: MatDialog,
    private propertyTypeService: PropertyTypeService,
    private userService: UsersService,
    private propertyService: PropertiesService,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<PropertyModalComponent>,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.property = this.data.property;
    this.currentUser = this.data.currentUser;
    this.propertyTypeService.propertyTypeList.subscribe(list => this.propertyTypes = list);
    this.userService.usersList.subscribe(list => this.users = list);

    this.landlordOptions = this.users.filter(x => x.role.name == "landlord").map(x => x.email);

    this.propertyForm = new FormGroup({      
      'parcel_number': new FormControl(this.property ? this.property.parcel_number : null, Validators.required),
      'block': new FormControl(this.property ? this.property.block : null, Validators.required),
      'type': new FormControl(this.property?.type.name, Validators.required),
      // 'landlord': new FormControl(this.property?.landlord[0]?.email, Validators.required)
    });

    this.propertyForm.addControl("landlord", this.formBuilder.control(this.property?.landlord[0]?.email))

    this.filteredLandlords = this.propertyForm.get('landlord').valueChanges
      .pipe(startWith(''), map(value => this._filter(value)));
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.landlordOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  addProperty(){
    if(this.propertyForm.valid){
      var fv = this.propertyForm.value

      var propertyDto: PropertyDTO = {
        parcel_number: +fv.parcel_number,
        block: fv.block,
        type_id: this.propertyTypes.find(x => x.name == fv.type).id,
        landlord_id: this.users.find(x => x.email == fv.landlord).id,
      }

      this.propertyService.addProperty(propertyDto)
        .then(() => {
          this.userService.fetchUsers();
          this.close();
        })
        .catch(e => this.toastr.error(e.error.message));
    }
    else this.toastr.error("One or more fields are incorrect!");
  }

  applyChanges(){
    if(this.propertyForm.valid){
      var fv = this.propertyForm.value

      var propertyDto: PropertyDTO = {
        parcel_number: fv.parcel_number,
        block: fv.block,
        type_id: this.propertyTypes.find(x => x.name == fv.type).id,
        landlord_id: this.users.find(x => x.email == fv.landlord).id
      }

      this.propertyService.updateProperty(this.property.id, propertyDto)
        .then(() => this.close())
        .catch(e => this.toastr.error(e.error.message));

      this.editing = false;
    }
    else this.toastr.error("One or more fields are incorrect!");
  }

  deleteProperty(){
    this.dialog.open(ConfirmModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        message: "Delete this property?"
      }
    })
    .afterClosed().subscribe(response => {
      if(response.data == "confirm"){
        this.propertyService.deleteProperty(this.property);
        this.dialog.closeAll()
      }
    });
  }

  openUser(userId: number){
    this.dialog.open(ViewEditModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        user: this.userService.selectUserFromList(userId),
        currentUser: this.currentUser
      }
    });
  }

  close() {
    this.dialogRef.close();
  }

}
