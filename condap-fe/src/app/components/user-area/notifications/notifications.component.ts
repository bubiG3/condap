import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { NotificationModel } from 'src/app/models/notification.model';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  notifications: NotificationModel[] = [];
  notificationExpanded: boolean = false;
  selectedNotification: NotificationModel = null;

  constructor(private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.notificationService.fetchNotifications();
    this.notificationService.notificationList.subscribe(list => this.notifications = list);
  }

  selectNotification(notification: NotificationModel){
    this.selectedNotification = notification;
    this.notificationExpanded = true;
    if(!notification.seen_state) this.notificationService.markAsSeen(notification);
  }

  close(){
    setTimeout(() => {
      this.selectedNotification = null;
      this.notificationExpanded = false;
    }, 10);
  }

  get IsFetching(){
    return this.notificationService.fetching;
  }

}
