import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CreateNotification } from 'src/app/models/DTOs/create-notification.model';
import { Role } from 'src/app/models/role.model';
import { NotificationService } from 'src/app/services/notification.service';
import { RolesService } from 'src/app/services/roles.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.scss']
})
export class NotificationModalComponent implements OnInit {

  createNotificationForm: FormGroup;
  roles: Role[] = [];
  selectedRoles: any[] = [];

  constructor(private roleService: RolesService,
    private notificationService: NotificationService,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<NotificationModalComponent>) { }

  ngOnInit(): void {
    this.createNotificationForm = new FormGroup({
      'title': new FormControl(null, Validators.required),
      'content': new FormControl(null, Validators.required),
    });

    this.roleService.rolesList.subscribe(list => this.roles = list);
  }

  onCheckChange(event, roleId){
    if (event.checked === true) {
      this.selectedRoles.push(roleId);
    }
    if (event.checked === false) {
        var index: number = this.selectedRoles.indexOf(roleId);
        this.selectedRoles.splice(index, 1);
    }
  }

  onSubmit(){
    if(this.createNotificationForm.valid) {
      if(this.selectedRoles.length > 0) {
        var rolesString: string = '[' + this.selectedRoles.toString() + ']';
        var notification: CreateNotification = {
          title: this.createNotificationForm.value.title,
          content: this.createNotificationForm.value.content,
          target_roles: rolesString
        }
        this.notificationService.createNotification(notification)
          .then(() => this.close())
          .catch(e => this.toastr.error(e.error.message));
      }
      else this.toastr.error("Please select at least one target role!");
    }
    else this.toastr.error("One or more fields are invalid!");
  }
  
  close() {
    this.dialogRef.close();
  }

}
