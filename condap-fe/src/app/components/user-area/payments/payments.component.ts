import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { customModalSize } from 'src/app/app.component';
import { Payment } from 'src/app/models/payment.model';
import { User } from 'src/app/models/user.model';
import { PaymentService } from 'src/app/services/payment.service';
import { ReceiptService } from 'src/app/services/receipt.service';
import { UsersService } from 'src/app/services/users.service';
import { ViewEditModalComponent } from '../users/view-edit-modal/view-edit-modal.component';
import { UploadPaymentModalComponent } from './upload-payment-modal/upload-payment-modal.component';
import { ViewPaymentComponent } from './view-payment/view-payment.component';

declare var require: any
const FileSaver = require('file-saver');

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  page: number = 1;
  @Input() currentUser: User;

  payments: Payment[];
  asc: Boolean = null;
  sortBy: string = null;

  constructor(public dialog: MatDialog,
    public datePipe: DatePipe,
    private paymentService: PaymentService,
    private userService: UsersService,
    private receiptService: ReceiptService) { }

  ngOnInit(): void {
    this.paymentService.paymentList.subscribe(list => this.payments = list);
  }
  
  openUploadPaymentModal(){
    this.dialog.open(UploadPaymentModalComponent, customModalSize);
  }
  
  openViewPaymentModal(payment: Payment){
    this.dialog.open(ViewPaymentComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        payment: payment,
        currentUser: this.currentUser
      }
    });
  }

  openUserModal(userId: number){
    this.dialog.open(ViewEditModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        user: this.userService.selectUserFromList(userId),
        currentUser: this.currentUser
      }
    });
  }

  get IsFetching() {
    return this.paymentService.fetching;
  }

  downloadReceipt(payment: Payment){
    this.receiptService.downloadReceipt(payment.id).subscribe(data => {
      const fileName = payment.property.parcel_number + payment.property.block + "-" +  
        this.datePipe.transform(payment.start_date, 'dd.MM.yyyy') + "-" + 
        this.datePipe.transform(payment.end_date, 'dd.MM.yyyy') + ".pdf";
      FileSaver.saveAs(data.images, fileName);
    })
  }

  sort(param: string){
    this.sortBy = param;
    if(this.asc == null){
      this.asc = false;
    }
    
    if(param == "id"){
      if(this.asc){
        this.payments.sort((a,b) => {
          return b.property.parcel_number.toString().concat(b.property.block + " - " + 
            this.datePipe.transform(b.start_date, 'MM/yyyy').toString() + " - " + 
            this.datePipe.transform(b.end_date, 'MM/yyyy').toString())
          .localeCompare(a.property.parcel_number.toString().concat(a.property.block + " - " + 
            this.datePipe.transform(a.start_date, 'MM/yyyy').toString() + " - " + 
            this.datePipe.transform(a.end_date, 'MM/yyyy').toString()));
        });
        this.asc = false;
      }
      else{
        this.payments.sort((a,b) => {
          return a.property.parcel_number.toString().concat(a.property.block + " - " + 
            this.datePipe.transform(a.start_date, 'MM/yyyy').toString() + " - " + 
            this.datePipe.transform(a.end_date, 'MM/yyyy').toString())
          .localeCompare(b.property.parcel_number.toString().concat(b.property.block + " - " + 
            this.datePipe.transform(b.start_date, 'MM/yyyy').toString() + " - " + 
            this.datePipe.transform(b.end_date, 'MM/yyyy').toString()));
        });
        this.asc = true;
      }
    }
    if(param == "property"){
      if(this.asc){
        this.payments.sort((a,b) => b.property.parcel_number.toString().concat(b.property.block).localeCompare(a.property.parcel_number.toString().concat(a.property.block)));
        this.asc = false;
      }
      else{
        this.payments.sort((a,b) => a.property.parcel_number.toString().concat(a.property.block).localeCompare(b.property.parcel_number.toString().concat(b.property.block)));
        this.asc = true;
      }
    }
    if(param == "name"){
      if(this.asc){
        this.payments.sort((a,b) => b.user.first_name.localeCompare(a.user.first_name));
        this.asc = false;
      }
      else{
        this.payments.sort((a,b) => a.user.first_name.localeCompare(b.user.first_name));
        this.asc = true;
      }
    }
    if(param == "status"){
      if(this.asc){
        this.payments.sort((a,b) => b.status.name.localeCompare(a.status.name));
        this.asc = false;
      }
      else{
        this.payments.sort((a,b) => a.status.name.localeCompare(b.status.name));
        this.asc = true;
      }
    }
    if(param == "date"){
      if(this.asc){
        this.payments.sort((a,b) => Number(new Date(b.created_at)) - Number(new Date(a.created_at)));
        this.asc = false;
      }
      else{
        this.payments.sort((a,b) => Number(new Date(a.created_at)) - Number(new Date(b.created_at)));
        this.asc = true;
      }
    }
  }
}
