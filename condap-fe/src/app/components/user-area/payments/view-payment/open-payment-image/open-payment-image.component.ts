import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-open-payment-image',
  templateUrl: './open-payment-image.component.html',
  styleUrls: ['./open-payment-image.component.scss']
})
export class OpenPaymentImageComponent implements OnInit {

  file: {
    name: string;
    url: string;
    type: string;
  }

  constructor(private dialogRef: MatDialogRef<OpenPaymentImageComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
    dialogRef.disableClose = true;
    this.file = data.file;
  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

}
