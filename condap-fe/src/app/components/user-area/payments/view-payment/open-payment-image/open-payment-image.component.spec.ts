import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenPaymentImageComponent } from './open-payment-image.component';

describe('OpenPaymentImageComponent', () => {
  let component: OpenPaymentImageComponent;
  let fixture: ComponentFixture<OpenPaymentImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenPaymentImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenPaymentImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
