import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-rejected-reason',
  templateUrl: './rejected-reason.component.html',
  styleUrls: ['./rejected-reason.component.scss']
})
export class RejectedReasonComponent implements OnInit {

  rejectPaymentReasonForm: FormGroup;

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<RejectedReasonComponent>,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.initForm();
  }

  reject(){
    this.rejectPaymentReasonForm.value.message ? 
      this.dialogRef.close({data: this.rejectPaymentReasonForm.value.message}) :
      this.toastr.error("Please, provide a reason!");
  }

  close(){
    this.dialogRef.close();
  }

  private initForm(){
    this.rejectPaymentReasonForm = this.fb.group({
      message: [null],
    });
  }

}
