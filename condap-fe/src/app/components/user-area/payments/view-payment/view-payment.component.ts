import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { customModalSize } from 'src/app/app.component';
import { Payment } from 'src/app/models/payment.model';
import { User } from 'src/app/models/user.model';
import { PaymentService } from 'src/app/services/payment.service';
import { OpenPaymentImageComponent } from './open-payment-image/open-payment-image.component';
import { RejectedReasonComponent } from './rejected-reason/rejected-reason.component';

@Component({
  selector: 'app-view-payment',
  templateUrl: './view-payment.component.html',
  styleUrls: ['./view-payment.component.scss']
})
export class ViewPaymentComponent implements OnInit {

  payment: Payment;
  currentUser: User;
  image: string;
  files: {
    name: string;
    url: string;
    type: string;
    seen: boolean;
  }[] = [];
  fetchingFile: boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
              private paymentService: PaymentService,
              public dialog: MatDialog,
              private dialogRef: MatDialogRef<ViewPaymentComponent>,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.payment = this.data.payment;
    this.currentUser = this.data.currentUser;
    this.loadImages();
  }

  private loadImages(){
    var i = 1;
    this.payment.pictures.forEach((file: any) => { 
      this.fetchingFile = true;
      this.paymentService.fetchPaymentImage(file.name).subscribe(file => {
        this.files.push({name: "File" + i++, url: file.images, type: file.images.split('.').pop(), seen: false})
        this.fetchingFile = false;
      })
    })
  }

  openPaymentRefenceFile(file: any){
    file.seen = true;
    this.dialog.open(OpenPaymentImageComponent, {
      width: "95vw",
      maxWidth: "70rem",
      height: "95vh",
      maxHeight: "90vh",
      data: {
        file: file
      }
    });
  }

  approvePayment(){
    this.paymentService.approvePayment(this.payment)
      .then(() => this.close())
      .catch(e => this.toastr.error(e.error.message));
  }

  rejectPayment(){
    this.dialog.open(RejectedReasonComponent, customModalSize)
      .afterClosed().subscribe(response => {
        if(response?.data){
          this.payment.message = response.data;
          this.paymentService.rejectPayment(this.payment)
            .then(() => this.close())
            .catch(e => this.toastr.error(e.error.message));
        }
      })
  }

  close(){
    this.dialogRef.close();
  }

}
