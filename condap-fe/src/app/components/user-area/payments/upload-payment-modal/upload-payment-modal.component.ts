import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CalendarView } from '@syncfusion/ej2-angular-calendars';
import { Observable } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';
import { PaymentDTO } from 'src/app/models/DTOs/payment-dto.model';
import { Property } from 'src/app/models/property.model';
import { PaymentService } from 'src/app/services/payment.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-upload-payment-modal',
  templateUrl: './upload-payment-modal.component.html',
  styleUrls: ['./upload-payment-modal.component.scss']
})
export class UploadPaymentModalComponent implements OnInit {
  public start: CalendarView = 'Year';
  public depth: CalendarView = 'Year';
  public format: string = 'MMM yyyy';
  minDate: Date;

  properties: Property[];

  uploadPaymentForm: FormGroup;

  options: string[];
  filteredOptions: Observable<string[]>;

  fileToUpload: File = null;
  filesToUpload: File[] = [];
  @ViewChild('uploader') uploader: ElementRef;

  constructor(private propertyService: PropertiesService,
              private paymentService: PaymentService,
              private toastr: ToastrService,
              private dialogRef: MatDialogRef<UploadPaymentModalComponent>) { }

  ngOnInit(): void {
    this.uploadPaymentForm = new FormGroup({      
      'image': new FormControl(null, Validators.required),
      'property': new FormControl(null, Validators.required),
      'date': new FormControl(null, Validators.required),
    });

    this.uploadPaymentForm.get("property").valueChanges.subscribe(prop => {
      var property = this.propertyService.propertyList.value.find(x => 
        x.parcel_number.toString().concat(x.block) == prop);
      var lastDate = new Date(property?.last_payment_date);
      lastDate.setDate(lastDate.getDate() + 1);
      this.minDate = lastDate;
    })

    this.propertyService.propertyList.subscribe(list => this.properties = list);

    this.options = this.properties.map(x => x.parcel_number.toString().concat(x.block));
    this.filteredOptions = this.uploadPaymentForm.get('property').valueChanges
      .pipe(startWith(''), map(value => this._filter(value)));
  }

  submitProof(){
    if(this.uploadPaymentForm.valid){
      if(this.filesToUpload.length <= 3){
        var fv = this.uploadPaymentForm.value;

        var date1: Date = new Date(this.uploadPaymentForm.value.date[0]);
        var date2: Date = new Date(this.minDate);

        if(date1.setHours(0,0,0,0) != date2.setHours(0,0,0,0)){
          this.toastr.error("Previous month(s) not paid!");
        }
        else{
          var paymentDto: PaymentDTO = {
            property_id: this.properties.find(x => x.parcel_number.toString().concat(x.block) == fv.property).id,
            pictures: this.filesToUpload,
            start_date: fv.date[0],
            end_date: fv.date[1]
          }

          this.paymentService.uploadPayment(paymentDto)
            .then(() => this.close())
            .catch(e => this.toastr.error(e.error.message));
        }

      }
      else this.toastr.error("Maximum number of files is 3!"); 
    }
    else this.toastr.error("One or more fields are invalid!");
  }

  handleFile(event){
    for (var i = 0; i < event.target.files.length; i++) { 
      this.filesToUpload.push(event.target.files[i]);
    }
  }

  reset(){
    this.uploader.nativeElement.value = "";
    this.filesToUpload = [];
  }

  close(){
    this.dialogRef.close();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
}
