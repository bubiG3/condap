import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPaymentModalComponent } from './upload-payment-modal.component';

describe('ModalComponent', () => {
  let component: UploadPaymentModalComponent;
  let fixture: ComponentFixture<UploadPaymentModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadPaymentModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPaymentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
