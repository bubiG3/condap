import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Role } from 'src/app/models/role.model';
import { UserDTO } from 'src/app/models/DTOs/user-dto.model';
import { User } from 'src/app/models/user.model';
import { RolesService } from 'src/app/services/roles.service';
import { UsersService } from 'src/app/services/users.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { PropertyModalComponent } from '../../properties/property-modal/property-modal.component';
import { PropertiesService } from 'src/app/services/properties.service';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Property } from 'src/app/models/property.model';

@Component({
  selector: 'app-view-edit-modal',
  templateUrl: './view-edit-modal.component.html',
  styleUrls: ['./view-edit-modal.component.scss']
})
export class ViewEditModalComponent implements OnInit {

  editing: boolean = false;
  updateUserForm: FormGroup;
  user: User;
  currentUser: User;
  roles: Role[];
  image$: Observable<string>;

  properties: Property[];
  propertyOptions: string[];
  filteredProperties: Observable<string[]>;
  loadProperties: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(@Inject(MAT_DIALOG_DATA) public data,
    public dialog: MatDialog,
    private userService: UsersService,
    private roleService: RolesService,
    private propertyService: PropertiesService,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<ViewEditModalComponent>,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.user = this.data.user;
    this.currentUser = this.data.currentUser;

    this.roleService.rolesList.subscribe(list => this.roles = list);
    this.propertyService.propertyList.subscribe(list => this.properties = list);

    this.initForm();
    this.loadProperties.subscribe(value => {
      if(value) {
        this.filteredProperties = this.updateUserForm?.get('property')?.valueChanges
        .pipe(startWith(''), map(value => this._filter(value)));
      }
    })
    this.propertyOptions = this.properties?.map(x => x.parcel_number.toString().concat(x.block));

    this.updateUserForm.get("role").valueChanges.subscribe(role => {
      if(role == "tenant" || role == "occupant"){
        this.updateUserForm.addControl('property', 
          new FormControl(
            this.user.property?.length > 0 ? 
              this.user.property[0].parcel_number.toString().concat(this.user.property[0].block) : 
              null, 
            Validators.required
          ));
        this.loadProperties.next(true);
      }
      else{
        this.updateUserForm.removeControl('property');
        this.loadProperties.next(false);
      }
    })

    this.image$ = this.userService.fetchSelectedUserPicture(this.user).pipe(map(x => x.images));
  }

  startEdit(){
    this.editing = true;
  }

  cancelEdit(){
    this.editing = false;
  }

  applyChanges(){
    if(this.updateUserForm.valid){
      var fv = this.updateUserForm.value;
      
      var userDto: UserDTO = {
        first_name: fv.firstName,
        last_name: fv.lastName,
        ci: fv.ci,
        email: fv.email,
        phone: fv.phone,
        dob: fv.dob,
        role_id: this.roles.find(x => x.name == fv.role).id
      }
      fv.property && (userDto.property_id = this.properties.find(x => x.parcel_number.toString().concat(x.block) == fv.property).id);

      this.userService.updateUser(this.user.id, userDto)
        .then(() => this.close())
        .catch(e => this.toastr.error(e.error.message))
      this.editing = false;
    }
    else this.toastr.error("One or more fields are invalid!");
  }

  deleteUser(message: string){
    this.dialog.open(ConfirmModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        message: message
      }
    }).afterClosed().subscribe(response => {
      if(response.data == "confirm"){
        this.userService.deleteUser(this.data.user);
        this.dialog.closeAll()
      }
    });
  }
  
  makeInactive(message: string){
    this.dialog.open(ConfirmModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        message: message
      }
    }).afterClosed().subscribe(response => {
      if(response.data == "confirm"){
        this.userService.updateUserStatus(this.user.id);
        this.dialog.closeAll()
      }
    });
  }
  
  makeActive(message: string){
    this.dialog.open(ConfirmModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        message: message
      }
    }).afterClosed().subscribe(response => {
      if(response.data == "confirm"){
        this.userService.updateUserStatus(this.user.id);
        this.dialog.closeAll()
      }
    });
  }

  openProperty(propertyId: number){
    this.dialog.open(PropertyModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        action: "view",
        property: this.propertyService.selectPropertyFromList(propertyId),
        currentUser: this.currentUser
      }
    });
  }

  private initForm(){
    this.updateUserForm = this.fb.group({
      firstName: [this.user.first_name, Validators.required],
      lastName: [this.user.last_name, Validators.required],
      dob: [this.user.dob, Validators.required],
      ci: [this.user.ci, Validators.required],
      phone: [this.user.phone, Validators.required],
      email: [this.user.email, Validators.required],
      role: [this.user.role.name, Validators.required]
    });
    if(this.user.role.name == "tenant" || this.user.role.name == "occupant"){
      this.updateUserForm.addControl("property", 
        new FormControl(
          this.user.property?.length > 0 ? 
            this.user.property[0].parcel_number.toString().concat(this.user.property[0].block) : 
            null, 
          Validators.required
        )
      );
      this.loadProperties.next(true);
    }
  }

  private close() {
    this.dialogRef.close();
  }
  
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.propertyOptions?.filter(option => option.toLowerCase().includes(filterValue));
  }

}
