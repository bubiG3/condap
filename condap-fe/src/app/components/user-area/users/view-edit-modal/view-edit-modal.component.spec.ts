import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEditModalComponent } from './view-edit-modal.component';

describe('ViewEditModalComponent', () => {
  let component: ViewEditModalComponent;
  let fixture: ComponentFixture<ViewEditModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewEditModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
