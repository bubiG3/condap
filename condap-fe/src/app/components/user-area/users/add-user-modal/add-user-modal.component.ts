import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Role } from 'src/app/models/role.model';
import { UserDTO } from 'src/app/models/DTOs/user-dto.model';
import { RolesService } from 'src/app/services/roles.service';
import { UsersService } from 'src/app/services/users.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Property } from 'src/app/models/property.model';
import { PropertiesService } from 'src/app/services/properties.service';
import { map, startWith } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.scss']
})
export class AddUserModalComponent implements OnInit {

  addUserForm: FormGroup;
  roles: Role[];

  properties: Property[];
  propertyOptions: string[];
  filteredProperties: Observable<string[]>;
  loadProperties: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private rolesService: RolesService, 
              private userService: UsersService,
              private propertyService: PropertiesService,
              private toastr: ToastrService,
              private dialogRef: MatDialogRef<AddUserModalComponent>,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();

    this.rolesService.rolesList.subscribe(list => this.roles = list);
    this.propertyService.propertyList.subscribe(list => {
      this.properties = list;
    });
    
    this.propertyOptions = this.properties?.map(x => x.parcel_number.toString().concat(x.block));
    
    this.loadProperties.subscribe(value => {
      if(value) {
        this.filteredProperties = this.addUserForm.get('property').valueChanges
        .pipe(startWith(''), map(value => this._filter(value)));
      }
    })

    this.addUserForm.get("role").valueChanges.subscribe(role => {
      if(role == "tenant" || role == "occupant"){
        this.addUserForm.addControl('property', new FormControl(null, Validators.required));
        this.loadProperties.next(true);
      }
      else{
        this.addUserForm.removeControl('property');
        this.loadProperties.next(false);
      }
    })
  }

  onSubmit() {
    if (this.addUserForm.valid) {
      var fv = this.addUserForm.value;
      
      var userDto: UserDTO = {
        first_name: fv.firstName[0].toUpperCase() + fv.firstName.substr(1).toLowerCase(),
        last_name: fv.lastName[0].toUpperCase() + fv.lastName.substr(1).toLowerCase(),
        email: fv.email,
        phone: fv.phone,
        dob: fv.dob,
        ci: fv.ci,
        role_id: this.roles.filter(x => x.name == fv.role)[0].id
      };
      if(fv.property) userDto.property_id = this.properties.find(x => x.parcel_number.toString().concat(x.block) == fv.property).id;

      this.userService.createUser(userDto)
        .then(() => this.close())
        .catch(e => this.toastr.error(e.error.message))
    }
    else this.toastr.error("One or more fields are invalid!");
  }

  close() {
    this.dialogRef.close();
  }

  private initForm(){
    this.addUserForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dob: ['', Validators.required],
      ci: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      role: ['', Validators.required]
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.propertyOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

}
