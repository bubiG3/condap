import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { customModalSize } from 'src/app/app.component';
import { User } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';
import { AddUserModalComponent } from './add-user-modal/add-user-modal.component';
import { ViewEditModalComponent } from './view-edit-modal/view-edit-modal.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  page: number = 1;
  @Input() currentUser: User;

  users: User[];
  inactiveUsersLength: number;
  asc: Boolean = null;
  sortBy: string = null;

  constructor(public dialog: MatDialog, private usersService: UsersService) { }

  ngOnInit(): void {
    this.usersService.usersList.subscribe(list => this.users = list);
    this.usersService.usersList.subscribe(list => this.inactiveUsersLength = list?.filter(x => x.active == false).length);
  }

  openAddUserModal(){
    this.dialog.open(AddUserModalComponent, customModalSize);
  }

  openViewEditModal(user: User){
    this.dialog.open(ViewEditModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        user: user,
        currentUser: this.currentUser
      }
    });
  }

  get IsFetching() {
    return this.usersService.fetching;
  }

  sort(param: string){
    this.sortBy = param;
    if(this.asc == null){
      this.asc = false;
    }
    if(param == "name"){
      if(this.asc){
        this.users.sort((a,b) => b.first_name.localeCompare(a.first_name));
        this.asc = false;
      }
      else{
        this.users.sort((a,b) => a.first_name.localeCompare(b.first_name));
        this.asc = true;
      }
    }
    if(param == "email"){
      if(this.asc){
        this.users.sort((a,b) => b.email.localeCompare(a.email));
        this.asc = false;
      }
      else{
        this.users.sort((a,b) => a.email.localeCompare(b.email));
        this.asc = true;
      }
    }
    if(param == "role"){
      if(this.asc){
        this.users.sort((a,b) => b.role.name.localeCompare(a.role.name));
        this.asc = false;
      }
      else{
        this.users.sort((a,b) => a.role.name.localeCompare(b.role.name));
        this.asc = true;
      }
    }
    if(param == "phone"){
      if(this.asc){
        this.users.sort((a,b) => b.phone.localeCompare(a.phone));
        this.asc = false;
      }
      else{
        this.users.sort((a,b) => a.phone.localeCompare(b.phone));
        this.asc = true;
      }
    }
  }
}
