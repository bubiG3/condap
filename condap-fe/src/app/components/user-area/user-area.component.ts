import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/models/user.model';
import { NotificationService } from 'src/app/services/notification.service';
import { PaymentService } from 'src/app/services/payment.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { UsersService } from 'src/app/services/users.service';
import { ToastrService } from 'ngx-toastr';
import { ChangePasswordModalComponent } from './change-password-modal/change-password-modal.component';
import { customModalSize } from 'src/app/app.component';

@Component({
  selector: 'app-user-area',
  templateUrl: './user-area.component.html',
  styleUrls: ['./user-area.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserAreaComponent implements OnInit {

  showUpdateProfilePicture: boolean = false;
  currentUser: User;
  profilePicture: string;
  labels: string[] = [];
  private labelStrings: string[] = ['payments', 'properties', 'users', 'improvements'];

  constructor(private usersService: UsersService, public dialog: MatDialog,
    private propertyService: PropertiesService,
    private paymentService: PaymentService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private toastr: ToastrService) {}

  ngOnInit(): void {
    this.usersService.loggedUser.subscribe(user => this.currentUser = user);
    this.usersService.fetchCurrentUser();
    this.notificationService.fetchNotifications();
    this.paymentService.fetchPayments();
    this.usersService.fetchUsers();
    this.propertyService.fetchProperties();
    this.labelStrings.forEach(item => {
      this.labels.push(this.translateService.instant(item));
    });
  }

  get IsFetching() {
    return this.usersService.fetchingCurrent;
  }
  
  get IsFetchingPicture() {
    return this.usersService.fetchingProfilePicture;
  }

  uploadProfilePicture(event){
    this.usersService.uploadProfilePicture(event.target.files[0]).subscribe(() => {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.currentUser.picture = event.target.result.toString();
      }
      this.toastr.success("Profile picture updated successfully!");
    },
    e => this.toastr.error(e.error.message));    
  }
  
  changePassword(){
    this.dialog.open(ChangePasswordModalComponent, customModalSize);
  }

}
