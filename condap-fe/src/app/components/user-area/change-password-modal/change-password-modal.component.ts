import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ChangePassword } from 'src/app/models/DTOs/change-password.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-change-password-modal',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.scss']
})
export class ChangePasswordModalComponent implements OnInit {

  changePasswordForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<ChangePasswordModalComponent>,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private authService: AuthService) { }

  ngOnInit(): void {
    this.initForm();
  }

  close() {
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.changePasswordForm.valid) {
      var fv = this.changePasswordForm.value;
      
      if(fv.newPassword != fv.confirmPassword){
        this.toastr.error("Passwords do not match!");
      }
      else{
        var changePassword: ChangePassword = {
          old_password: fv.oldPassword,
          new_password: fv.newPassword
        }
        this.authService.changePassword(changePassword).subscribe(data => {
          this.toastr.success("Password changed successfully!");
          this.close();
        },
        e => this.toastr.error(e.error.message))
      }

    }
    else this.toastr.error("One or more fields are invalid!");
  }

  private initForm(){
    this.changePasswordForm = this.fb.group({
      oldPassword: [null, Validators.required],
      newPassword: [null, Validators.required],
      confirmPassword: [null, Validators.required]
    });
  }

}
