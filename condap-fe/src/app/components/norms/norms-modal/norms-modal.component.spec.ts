import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NormsModalComponent } from './norms-modal.component';

describe('NormsModalComponent', () => {
  let component: NormsModalComponent;
  let fixture: ComponentFixture<NormsModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NormsModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NormsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
