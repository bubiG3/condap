import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { CreateNorms } from 'src/app/models/DTOs/create-norms.model';
import { NormService } from 'src/app/services/norm.service';

@Component({
  selector: 'app-norms-modal',
  templateUrl: './norms-modal.component.html',
  styleUrls: ['./norms-modal.component.scss']
})
export class NormsModalComponent implements OnInit {

  normsForm: FormGroup;
  fileToUpload: File = null;

  constructor(private normService: NormService,
    private toastr: ToastrService,
    private dialogRef: MatDialogRef<NormsModalComponent>) { }

  ngOnInit(): void {
    this.normsForm = new FormGroup({      
      'title': new FormControl(null, Validators.required),
      'file': new FormControl(null, Validators.required),
    });
  }

  handleFile(event){
    this.fileToUpload = event.target.files[0];
  }

  uploadNorms(){
    if(this.normsForm.valid){
      var norms: CreateNorms = {title: this.normsForm.value.title, file: this.fileToUpload}
      this.normService.uploadNewNorms(norms)
        .then(() => this.close())
        .catch(e => this.toastr.error(e.error.message));
    }
    else this.toastr.error("One or more fields are invalid!");
  }

  close(){
    this.dialogRef.close();
  }

}
