import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { customModalSize } from 'src/app/app.component';
import { Norm } from 'src/app/models/norm.model';
import { User } from 'src/app/models/user.model';
import { NormService } from 'src/app/services/norm.service';
import { UsersService } from 'src/app/services/users.service';
import { ConfirmModalComponent } from '../user-area/confirm-modal/confirm-modal.component';
import { NormsModalComponent } from './norms-modal/norms-modal.component';

declare var require: any
const FileSaver = require('file-saver');

@Component({
  selector: 'app-norms',
  templateUrl: './norms.component.html',
  styleUrls: ['./norms.component.scss']
})
export class NormsComponent implements OnInit {

  norms: Norm[];
  selectedNorm: Norm;
  fileUrl: string = '';
  fetchingFile: boolean = false;
  currentUser: User;

  constructor(private normService: NormService,
    public dialog: MatDialog,
    private userService: UsersService) { }

  ngOnInit(): void {
    this.normService.fetchNorms();
    this.userService.fetchCurrentUser();
    this.fetchingFile = true;
    this.normService.normsList.subscribe(list => {
      this.norms = list;
      this.selectedNorm = list[0];
      this.normService.fetchNormsFile(this.selectedNorm?.file_name).subscribe(data => {
        this.fileUrl = data.images;
        this.fetchingFile = false;
      });
    });
    this.userService.loggedUser.subscribe(user => this.currentUser = user);
  }

  changeSelected(normId: number){
    this.selectedNorm = this.norms.find(x => x.id == normId);
    this.fetchingFile = true;
    this.normService.fetchNormsFile(this.selectedNorm?.file_name).subscribe(data => {
      this.fileUrl = data.images;
      this.fetchingFile = false;
    });
  }

  openUploadNormsModal(){
    this.dialog.open(NormsModalComponent, customModalSize);
  }

  deleteNorms(){
    this.dialog.open(ConfirmModalComponent, {
      width: "95vw",
      maxWidth: "40rem",
      maxHeight: "90vh",
      data: {
        message: "Delete norms?"
      }
    }).afterClosed().subscribe(response => {
      if(response.data == "confirm"){
        this.normService.deleteNorms(this.selectedNorm.id);
        this.dialog.closeAll();
      }
    });
  }

  downloadNorms() {
    const fileName = this.selectedNorm.file_name + ".pdf";
    FileSaver.saveAs(this.fileUrl, fileName);
  }

  get IsFetching() {
    return this.normService.fetching;
  }

}
