import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { customModalSize } from 'src/app/app.component';
import { NotificationModel } from 'src/app/models/notification.model';
import { User } from 'src/app/models/user.model';
import { NotificationService } from 'src/app/services/notification.service';
import { UsersService } from 'src/app/services/users.service';
import { NotificationModalComponent } from '../../user-area/notifications/notification-modal/notification-modal.component';

@Component({
  selector: 'app-notification-panel',
  templateUrl: './notification-panel.component.html',
  styleUrls: ['./notification-panel.component.scss']
})
export class NotificationPanelComponent implements OnInit {

  @Output() closePanelEvent = new EventEmitter<boolean>();

  latestNotifications: NotificationModel[];
  loggedUser: User;
  language: string;

  constructor(private notificationService: NotificationService,
    public dialog: MatDialog,
    private userService: UsersService,
    private translate: TranslateService) { }

  ngOnInit(): void {
    this.notificationService.notificationList
      .pipe(
        map(list => list.filter(x => x.seen_state == false)), 
        map(list => list.slice(0, 5))
      )
      .subscribe(list => this.latestNotifications = list);

    this.userService.loggedUser.subscribe(user => this.loggedUser = user);
    this.language = this.translate.currentLang;
  }

  markAsSeen(notification: NotificationModel){
    this.notificationService.markAsSeen(notification);
  }

  markAllAsSeen(){
    this.notificationService.markAllAsSeen();
  }

  openNotificationPanel(){
    this.dialog.open(NotificationModalComponent, customModalSize);
  }

  closePanel() {
    this.closePanelEvent.emit();
  }

  get IsFetching() {
    return this.notificationService.fetching;
  }

}
