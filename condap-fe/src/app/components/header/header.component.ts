import { Component, ElementRef, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  onMobile: boolean = false;
  navOpen: boolean = false;
  inPortal: boolean = false;
  isAuthenticated: boolean = false;
  showLogout: boolean = false;
  showNotifications: boolean = false;

  @Input() language: string = "en";
  userTokenSub: Subscription;
  notificationsCount: number = 0;

  @HostListener('document:click', ['$event'])
  clickout(event) {
    // If click outside -> hide notifications
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.showNotifications = false;
    }
  }

  constructor(private router: Router,
    private authService: AuthService, 
    private notificationService: NotificationService,
    private eRef: ElementRef) { }

  ngOnInit(): void {
    this.onMobile = window.innerWidth <= 959;
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd){
        if(event.url.includes("/portal")) this.inPortal = true;
        else this.inPortal = false;
      }
    });
    this.userTokenSub = this.authService.userToken.subscribe(token => {
      this.isAuthenticated = !!token;
    });

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.navOpen = false;
      }
    });

    // Get nr of unseen notifications
    this.notificationService.notificationList
      .pipe(map(list => list?.filter(x => x.seen_state == false)))
      .subscribe(list => {
        this.notificationsCount = list?.length;
      })
  }
  
  ngOnDestroy(): void {
    this.userTokenSub.unsubscribe();
  }

  logout(){
    this.authService.logout();
  }

  hideLogout(){
    setTimeout(() => {
      this.showLogout = false
    }, 1000);
  }

  hideNotifications(){
    setTimeout(() => {
      this.showNotifications = false
    }, 1000);
  }

  changeLanguage(){
    var url = this.router.url;
    url = url.substr(4, url.length);

    if(this.language == "en") {
      this.router.navigateByUrl('es/' + url)
    }
    else if(this.language == "es"){
      this.router.navigateByUrl('en/' + url)
    }
  }

  toggleNotifications(){
    this.showNotifications = !this.showNotifications;
    this.navOpen = false;
  }

  onResize(event){
    this.onMobile = event.target.innerWidth <= 959;
  }

}
