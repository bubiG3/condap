import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Credentials } from 'src/app/models/credentials.model';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private authService: AuthService,
    private toastr: ToastrService,
    private cookieService: CookieService,
    private router: Router,
    private translate: TranslateService) { } 

  ngOnInit(): void {
    if(this.cookieService.get('token') != '' && this.cookieService.get('token')){
      this.router.navigateByUrl(this.translate.currentLang + "/portal");
    }

    this.loginForm = new FormGroup({
      'email': new FormControl(null, Validators.compose([Validators.required, Validators.email])),
      'password': new FormControl(null, Validators.required)
    }, { updateOn: 'submit'});
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value as Credentials);
    }
    else this.toastr.error("One or more fields are invalid!")
  }
}
