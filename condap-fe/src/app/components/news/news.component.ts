import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  language: string;

  constructor(private translate: TranslateService) { }

  ngOnInit(): void {
    this.language = this.translate.currentLang;
  }

}
