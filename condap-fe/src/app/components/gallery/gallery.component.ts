import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  responsiveBreakpoint: number;
  responsiveBreakpointLands: number;
  responsiveSpan: number;

  constructor() { }

  ngOnInit(): void {
    this.responsiveBreakpoint = (window.innerWidth <= 800) ? 1 : 4;
    this.responsiveBreakpointLands = (window.innerWidth <= 800) ? 1 : 3;
    this.responsiveSpan = (window.innerWidth <= 800) ? 1 : 2;
  }

  onResize(event){
    this.responsiveBreakpoint = (event.target.innerWidth <= 800) ? 1 : 4;
    this.responsiveBreakpointLands = (event.target.innerWidth <= 800) ? 1 : 3;
    this.responsiveSpan = (event.target.innerWidth <= 800) ? 1 : 2;
  }

}
