import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sort-indicator',
  templateUrl: './sort-indicator.component.html',
  styleUrls: ['./sort-indicator.component.scss']
})
export class SortIndicatorComponent implements OnInit {

  @Input() isAscending: boolean = null;

  constructor() { }

  ngOnInit(): void {
  }

}
