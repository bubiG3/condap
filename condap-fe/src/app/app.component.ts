import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from './models/user.model';
import { UsersService } from './services/users.service';

export const customModalSize = {
  width: "95vw",
  maxWidth: "40rem",
  maxHeight: "90vh"
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],  
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {

  title = 'Condap';
  language: string = "en";
  currentUser: User;
  showWarning: boolean = false;

  constructor(private translate: TranslateService, private router: Router,
    private userService: UsersService) { 
    translate.addLangs(['en', 'es']);
    translate.setDefaultLang('en');
  }

  ngOnInit(){
    this.router.events.subscribe(val => {
      if (val instanceof RoutesRecognized) {
        var param = val.state.root.firstChild.params.language;
        if(param == "es") this.language = "es";
        else this.language = "en";
        this.translate.use(this.language);
       }
    });
    this.userService.loggedUser.subscribe(user => {
      if(new Date(user?.last_payment_date) < new Date()) this.showWarning = true;
      else this.showWarning = false;
    });

  }
  
}
