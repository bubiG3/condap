import { PaymentStatus } from "./payment-status.model";
import { Property } from "./property.model";
import { User } from "./user.model";

export class Payment{
    id: number;
    name?: string;
    user: User;
    property: Property;
    start_date: Date;
    end_date: Date;
    pictures: string[];
    status: PaymentStatus;
    created_at: Date;
    message?: string;
}