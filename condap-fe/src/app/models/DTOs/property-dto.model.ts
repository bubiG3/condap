export class PropertyDTO{
    parcel_number: number;
    block: string;
    type_id: number;
    landlord_id: number;
}