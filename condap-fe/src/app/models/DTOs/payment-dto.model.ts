export class PaymentDTO{
    property_id: number;
    pictures: File[];
    start_date: Date;
    end_date: Date;
}