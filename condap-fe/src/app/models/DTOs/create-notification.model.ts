export class CreateNotification{
    title: string;
    content: string;
    target_roles: string; // example format -> '[1,2,3,4]'
}