export class UserDTO{
    first_name: string;
    last_name: string;
    ci: number;
    email: string;
    phone: string;
    dob: Date;
    role_id: number;
    property_id?: number;
}