import { Property } from "./property.model";
import { Role } from "./role.model";

export class User{
    id: number;
    first_name: string;
    last_name: string;
    ci: number;
    email: string;
    phone: string;
    picture: string;
    dob: Date;
    role: Role;
    active: boolean;
    // In case of tenant
    property?: Property[];
    // In case of landlord
    properties?: Property[];
    last_payment_date?: Date;
}