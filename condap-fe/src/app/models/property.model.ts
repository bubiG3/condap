import { PropertyType } from "./property-type.model";
import { User } from "./user.model";

export class Property{
    id: number;
    parcel_number: number;
    block: string;
    property_number: string;
    type: PropertyType;
    landlord: User[];
    tenants: User[];
    occupants: User[];
    last_payment_date: Date;
    payment_status: string;
}