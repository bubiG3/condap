export class NotificationModel{
    id: number;
    title: string;
    content: string;
    created_at: Date;
    seen_state: boolean;
}