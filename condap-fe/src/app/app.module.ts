import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

// * Angular Material
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatBadgeModule } from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';

// * Other packages
import { FlexLayoutModule } from '@angular/flex-layout';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { CookieService } from 'ngx-cookie-service';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { HttpConfigInterceptor } from './services/interceptors/httpconfig.interceptor';
import { ToastrModule } from 'ngx-toastr';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgxPaginationModule } from 'ngx-pagination';

// * Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { UserAreaComponent } from './components/user-area/user-area.component';
import { PaymentsComponent } from './components/user-area/payments/payments.component';
import { UsersComponent } from './components/user-area/users/users.component';
import { ImprovementsComponent } from './components/user-area/improvements/improvements.component';
import { NormsComponent } from './components/norms/norms.component';
import { PropertiesComponent } from './components/user-area/properties/properties.component';
import { UploadPaymentModalComponent } from './components/user-area/payments/upload-payment-modal/upload-payment-modal.component';
import { AddUserModalComponent } from './components/user-area/users/add-user-modal/add-user-modal.component';
import { ViewEditModalComponent } from './components/user-area/users/view-edit-modal/view-edit-modal.component';
import { ConfirmModalComponent } from './components/user-area/confirm-modal/confirm-modal.component';
import { NewsComponent } from './components/news/news.component';
import { PropertyModalComponent } from './components/user-area/properties/property-modal/property-modal.component';
import { SelectedNewsComponent } from './components/selected-news/selected-news.component';
import { DatePipe } from '@angular/common';
import { ViewPaymentComponent } from './components/user-area/payments/view-payment/view-payment.component';
import { UserStatusPipePipe } from './pipes/user-status-pipe.pipe';
import { NotificationPanelComponent } from './components/header/notification-panel/notification-panel.component';
import { NotificationsComponent } from './components/user-area/notifications/notifications.component';
import { NotificationModalComponent } from './components/user-area/notifications/notification-modal/notification-modal.component';
import { OpenPaymentImageComponent } from './components/user-area/payments/view-payment/open-payment-image/open-payment-image.component';
import { NormsModalComponent } from './components/norms/norms-modal/norms-modal.component';
import { ChangePasswordModalComponent } from './components/user-area/change-password-modal/change-password-modal.component';
import { SortIndicatorComponent } from './components/shared/sort-indicator/sort-indicator.component';
import { RejectedReasonComponent } from './components/user-area/payments/view-payment/rejected-reason/rejected-reason.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "../assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ContactComponent,
    LoginComponent,
    GalleryComponent,
    UserAreaComponent,
    PaymentsComponent,
    UsersComponent,
    ImprovementsComponent,
    NormsComponent,
    PropertiesComponent,
    UploadPaymentModalComponent,
    AddUserModalComponent,
    ViewEditModalComponent,
    ConfirmModalComponent,
    NewsComponent,
    PropertyModalComponent,
    SelectedNewsComponent,
    ViewPaymentComponent,
    UserStatusPipePipe,
    NotificationPanelComponent,
    NotificationsComponent,
    NotificationModalComponent,
    OpenPaymentImageComponent,
    NormsModalComponent,
    ChangePasswordModalComponent,
    SortIndicatorComponent,
    RejectedReasonComponent,
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAy1MbbVLsvv_grlc3MXhwwZ3WaoeXWko4'
    }),
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      headerName: 'X-CSRF-TOKEN'
    }),
    FilterPipeModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    FlexLayoutModule,
    MatTabsModule,
    MatDialogModule,
    MatSelectModule,
    MatTableModule,
    ReactiveFormsModule,
    DateRangePickerModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatMenuModule,
    MatBadgeModule,
    MatIconModule,
    MatCheckboxModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
    }),
    MatProgressSpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    PdfViewerModule,
    NgxPaginationModule,
    MatDividerModule,
    Ng2SearchPipeModule,
    MatTooltipModule
  ],
  providers: [
    CookieService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
