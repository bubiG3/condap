import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './components/contact/contact.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { NormsComponent } from './components/norms/norms.component';
import { SelectedNewsComponent } from './components/selected-news/selected-news.component';
import { NotificationsComponent } from './components/user-area/notifications/notifications.component';
import { UserAreaComponent } from './components/user-area/user-area.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: ':language/home', component: HomeComponent, pathMatch: 'full' },
  { path: ':language/contact', component: ContactComponent },
  { path: ':language/login', component: LoginComponent },
  { path: ':language/gallery', component: GalleryComponent },
  { path: ':language/portal', component: UserAreaComponent, canActivate: [AuthGuard] },
  { path: ':language/portal/norms', component: NormsComponent, canActivate: [AuthGuard] },
  { path: ':language/portal/news', component: NewsComponent, canActivate: [AuthGuard] },
  { path: ':language/portal/news/selected', component: SelectedNewsComponent, canActivate: [AuthGuard] },
  { path: ':language/portal/notifications', component: NotificationsComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/en/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

