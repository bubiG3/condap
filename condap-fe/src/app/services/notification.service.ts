import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CreateNotification } from '../models/DTOs/create-notification.model';
import { NotificationModel } from '../models/notification.model';
import { RequestService } from './request/request.service';
import { SocketService } from './socket.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notificationList: BehaviorSubject<NotificationModel[]> = new BehaviorSubject<NotificationModel[]>([]);
  fetching: boolean = false;

  constructor(private requestService: RequestService,
    socketService: SocketService,
    private toastr: ToastrService) { 
    socketService.newNotification.subscribe(data => {if(data != null) this.handleNewNotification(data)});
  }

  fetchNotifications(){
    this.fetching = true;
    this.requestService.get("notifications").subscribe(data => {
      this.notificationList.next(data);
      this.fetching = false;
    })
  }

  createNotification(notification: CreateNotification){
    return this.requestService.post("notifications", notification).toPromise()
      .then(() => this.toastr.success("Succesfully broadcasted."));
  }

  private handleNewNotification(notification: NotificationModel){
    this.toastr.info("You have new notification!");
    this.fetchNotifications();
  }

  markAsSeen(notification: NotificationModel){
    this.requestService.post("notifications/" + notification.id)
      .subscribe(data => {
        var list: NotificationModel[] = this.notificationList.value;
        list.forEach(item => {
          if(item.id == data.id) item.seen_state = data.seen_state;
        });
        this.notificationList.next(list);
      });
  }
  
  markAllAsSeen(){
    this.requestService.post("notifications-see-all").subscribe(data => this.notificationList.next(data));
  }
  
}
