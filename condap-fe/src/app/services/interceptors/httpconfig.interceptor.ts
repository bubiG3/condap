import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor(private cookieService: CookieService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        const personalToken: string = this.cookieService.check('token')? this.cookieService.get('token') : null;
        const csrfToken: string = this.cookieService.check('XSRF-TOKEN')? this.cookieService.get('XSRF-TOKEN') : null;


        if (csrfToken) {
            request = request.clone({ headers: request.headers.set('X-XSRF-TOKEN', csrfToken) });

            if (personalToken) {
                request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + personalToken) });
            }    

            // if (!request.headers.has('Content-Type')) {
            //     request = request.clone({ headers: request.headers.set('Content-Type', 'undefined') });
            // }

            request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        }
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // console.log('event--->>>', event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };
                return throwError(error);
            }));
    }
}