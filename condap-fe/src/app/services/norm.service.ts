import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CreateNorms } from '../models/DTOs/create-norms.model';
import { Norm } from '../models/norm.model';
import { RequestService } from './request/request.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NormService {

  normsList: BehaviorSubject<Norm[]> = new BehaviorSubject<Norm[]>([]);
  selectedNorm: Norm;
  fetching: boolean = false;

  constructor(private requestService: RequestService,
    private toastr: ToastrService) { }

  fetchNorms(){
    this.fetching = true;
    this.requestService.get("norms").subscribe(data => {
      this.normsList.next(data);
      this.fetching = false;
    });
  }

  fetchNormsFile(fileName: string){
    return this.requestService.get("file/norm/" + fileName);
  }

  uploadNewNorms(norms: CreateNorms){
    var formData: FormData = new FormData();
    formData.append('title', norms.title);
    formData.append('file', norms.file);

    return this.requestService.post('norms', formData).toPromise()
      .then(data => {
        var list = this.normsList.value;
        list.unshift(data as Norm);
        this.normsList.next(list);
        this.toastr.success("New norms uploaded!");
      })
  }

  deleteNorms(normsId: number){
    this.requestService.delete("norms/", normsId).subscribe(data => {
      var list = this.normsList.value.filter(list => list.id != normsId);
      this.normsList.next(list);
      this.toastr.success("Norms deleted!");
    },
      e => this.toastr.error(e.error.message)
    )
  }
}
