import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Role } from '../models/role.model';
import { RequestService } from './request/request.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  rolesList: BehaviorSubject<Role[]> = new BehaviorSubject<Role[]>(null)

  constructor(private requestService: RequestService) { 
    this.fetchRoles();
  }

  fetchRoles(){
    this.requestService.get("user-roles").subscribe(data => this.rolesList.next(data));
  }
}
