import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PaymentDTO } from '../models/DTOs/payment-dto.model';
import { Payment } from '../models/payment.model';
import { RequestService } from './request/request.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PropertiesService } from './properties.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  paymentList: BehaviorSubject<Payment[]> = new BehaviorSubject<Payment[]>(null);
  fetching: boolean = false;

  constructor(private requestService: RequestService, 
    private datePipe: DatePipe,
    private toastr: ToastrService,
    private propertyService: PropertiesService) { }

  fetchPayments(){
    this.fetching = true;
    this.requestService.get("payments").subscribe(data => {
      this.paymentList.next(data);
      this.fetching = false;
    });
  }
  
  fetchPaymentImage(fileName: string){
    return this.requestService.get("file/payment/" + fileName);
  }

  uploadPayment(paymentDto: PaymentDTO){
    var formData: FormData = new FormData();
    formData.append('property_id', paymentDto.property_id.toString());
    paymentDto.pictures.forEach(pic => {
      formData.append('pictures[]', pic);
    });
    formData.append('start_date', this.datePipe.transform(paymentDto.start_date, 'yyyy-MM-dd'));
    formData.append('end_date', this.datePipe.transform(paymentDto.end_date, 'yyyy-MM-dd'));

    return this.requestService.post('payments', formData).toPromise()
      .then(data => {
        var list = this.paymentList.value;
        list.unshift(data as Payment);
        this.paymentList.next(list);
        this.toastr.success("Payment uploaded!");
      })
  }

  approvePayment(payment: Payment){
    var formData: FormData = new FormData();
    formData.append('_method', "PATCH");

    return this.requestService.post("payments/approve/" + payment.id, formData).toPromise()
      .then(data => {
        var list = this.paymentList.value;
        list.forEach(item => {
          if(item.id == data.id) item.status = data.status;
        });
        this.paymentList.next(list);
        this.toastr.success("Payment approved!");
      })
  }

  rejectPayment(payment: Payment){
    var formData: FormData = new FormData();
    formData.append('_method', "PATCH");
    payment.message && formData.append('message', payment.message);

    return this.requestService.post("payments/deny/" + payment.id, formData).toPromise()
      .then(data => {
        var list = this.paymentList.value;
        list.forEach(item => {
          if(item.id == data.id) item.status = data.status;
        });
        this.paymentList.next(list);
        this.propertyService.fetchProperties();
        this.toastr.success("Payment rejected!");
      })
  }

  get getPayments(){
    return this.paymentList.value;
  }
}
