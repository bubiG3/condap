import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserDTO } from '../models/DTOs/user-dto.model';
import { User } from '../models/user.model';
import { RequestService } from './request/request.service';
import { SocketService } from './socket.service';
import { ToastrService } from 'ngx-toastr';
import { PropertiesService } from './properties.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  usersList: BehaviorSubject<User[]> = new BehaviorSubject<User[]>(null);
  loggedUser: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  fetching: boolean = false;
  fetchingCurrent: boolean = false;
  fetchingProfilePicture: boolean = false;

  constructor(private requestService: RequestService,
    private socketService: SocketService,
    private toastr: ToastrService,
    private propertyService: PropertiesService) {}

  fetchUsers(){
    this.fetching = true;
    this.requestService.get("users").subscribe(
      data => {
        this.usersList.next(data);
        this.fetching = false;
      },
      () => this.toastr.error("Error getting users!")
    );
  }

  fetchCurrentUser(){
    this.fetchingCurrent = true;
    this.requestService.get("me").subscribe(data => {
      this.loggedUser.next(data);
      this.socketService.subscribeToNotifications(data);
      this.fetchingCurrent = false;
      this.fetchCurrentUserPicture();
    });
  }

  // logged in user
  fetchCurrentUserPicture(){
    this.fetchingProfilePicture = true;
    this.requestService.get("file/user/" + this.loggedUser.value.picture).subscribe(data => {
      var user: User = this.loggedUser.value;
      user.picture = data.images;
      this.loggedUser.next(user);
      this.fetchingProfilePicture = false;
    });
  }

  uploadProfilePicture(file: File){
    var formData: FormData = new FormData();
    formData.append('_method', "PATCH");
    formData.append('picture', file);

    return this.requestService.post("users/picture", formData)
  }
  
  // other users
  fetchSelectedUserPicture(user: User){
    return this.requestService.get("file/user/" + user.picture)
  }

  createUser(userDto: UserDTO){
    return this.requestService.post("users", userDto).toPromise().then(
      data => {
        var users: User[] = this.usersList.value;
        users.push(data.user as User);
        this.usersList.next(users);
        this.toastr.success("User created!");
        this.propertyService.fetchProperties();
      }
    )
  }

  updateUser(userId: number, userDto: UserDTO){
    return this.requestService.put("users/" + userId, userDto).toPromise().then(
      (data => {
        var users: User[] = this.usersList.value;
        users.forEach(user => {
          if(user.id == userId) user = data as User;
        })
        if(this.usersList.value == users) this.fetchUsers();
        this.usersList.next(users);
        this.toastr.success("User updated!");
        this.propertyService.fetchProperties();
      })
    )
  }

  deleteUser(user: User){
    this.requestService.delete("users/", user.id).subscribe(data => {
      var currentList: User[] = this.usersList.value.filter(obj => obj != user);
      this.usersList.next(currentList);
      this.toastr.success("User deleted!");
    },
      e => this.toastr.error(e.error.message)
    );
  }

  updateUserStatus(userId: number){
    var formData: FormData = new FormData();
    formData.append('_method', "PATCH");

    this.requestService.post("users/" + userId + "/status", formData).subscribe(data => {
      var users: User[] = this.usersList.value;
      users.forEach(user => {
        if(user.id == userId) user.active = data.active;
      })
      this.usersList.next(users);
      this.toastr.success("User status changed!");
      this.propertyService.fetchProperties();
    },
      e => this.toastr.error(e.error.message)
    )
  }

  selectUserFromList(userId: number){
    var users = this.usersList.value;
    return users.find(x => x.id == userId);
  }
}
