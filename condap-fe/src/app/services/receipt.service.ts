import { Injectable } from '@angular/core';
import { RequestService } from './request/request.service';

@Injectable({
  providedIn: 'root'
})
export class ReceiptService {

  constructor(private requestService: RequestService) { }

  downloadReceipt(paymentId: number){
    return this.requestService.get("file/receipt/" + paymentId);
  }
}
