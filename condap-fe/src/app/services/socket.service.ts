import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user.model';
import Echo from 'laravel-echo';
import { BehaviorSubject } from 'rxjs';
import { NotificationModel } from '../models/notification.model';
declare var require: any;

const PUSHER_API_KEY = 'local';
const PUSHER_CLUSTER = 'mt1';

@Injectable({
  providedIn: 'root',
})
export class SocketService {

  Pusher = require('pusher-js'); //maybe in constructor?
  echo: Echo;
  newNotification: BehaviorSubject<NotificationModel> = new BehaviorSubject<NotificationModel>(null);

  constructor() {
  }

  /**
   * Subscribe to common notifications.
   */
  subscribeToNotifications(user: User) {
    // console.log(user);
    // this.echo = new Echo({
    //   broadcaster: 'pusher',
    //   cluster: 'mt1 ',
    //   key: 'local',
    //   wsHost: '127.0.0.8',
    //   wsPort: 6001,
    //   disableStats: true,
    //   enabledTransports: ['ws'],
    //   forceTLS: false,
    // });
    // this.echo
    //   .channel(`${user.role.id}`)
    //   .listen('TestEvent', (response) => {
    //     console.log(response);
    //   });
    this.echo = new Echo({
      broadcaster: 'pusher',
      key: PUSHER_API_KEY,
      cluster: PUSHER_CLUSTER,
      wsHost: '127.0.0.8',
      wsPort: 6001,
      disableStats: true,
      enabledTransports: ['ws'],
      forceTLS: false,
    });
    this.echo.channel(`${user.role.id}`)
      .listen('TestEvent', (data) => {
        this.newNotification.next(data);
      });
  }

  /**
   * Unsubscribe from common notifications.
   * TODO: Call this where necessary.
   */
  unsubscribeFromNotifications(user: User) {
    this.echo.channel(`${user.role.id}`).stopListening('TestEvent');
  }
}
