import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PropertyType } from '../models/property-type.model';
import { RequestService } from './request/request.service';

@Injectable({
  providedIn: 'root'
})
export class PropertyTypeService {

  propertyTypeList: BehaviorSubject<PropertyType[]> = new BehaviorSubject<PropertyType[]>(null);

  constructor(private requestService: RequestService) { 
    this.fetchPropertyTypes();
  }

  fetchPropertyTypes(){
    this.requestService.get("property-types").subscribe(data => this.propertyTypeList.next(data));
  }
}
