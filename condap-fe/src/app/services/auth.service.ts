import { Injectable } from '@angular/core';
import { RequestService } from './request/request.service';
import { Credentials } from '../models/credentials.model';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { UsersService } from './users.service';
import { PropertiesService } from './properties.service';
import { PaymentService } from './payment.service';
import { SocketService } from './socket.service';
import { ToastrService } from 'ngx-toastr';
import { NotificationService } from './notification.service';
import { TranslateService } from '@ngx-translate/core';
import { ChangePassword } from '../models/DTOs/change-password.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userToken: BehaviorSubject<string> = new BehaviorSubject<string>(null)

  constructor(private requestService: RequestService, 
    private cookieService: CookieService, 
    private router: Router,
    private userService: UsersService,
    private propertyService: PropertiesService,
    private paymentsService: PaymentService,
    private socketService: SocketService,
    private notificationService: NotificationService,
    private toastr: ToastrService,
    private translate: TranslateService) { 
    if(this.cookieService.check("token")){
      this.userToken.next(this.cookieService.get("token"));
    }
  }

  login(credentials: Credentials){
      return this.requestService.post('users/login', credentials).subscribe(
      (data: any) => {
        if (data.accessToken) {
          //  ------------------------------------> Exp date?
          this.cookieService.set('token', data.accessToken);
          this.router.navigateByUrl(this.translate.currentLang + '/portal');
          this.userToken.next(data.accessToken);
          this.userService.loggedUser.next(data.user);
          this.toastr.success("Success");
        }      
      },
      (e: any) => {
        this.toastr.error(e.error.message);
      }
    );    
  }

  logout(){
    return this.requestService.post('users/logout').subscribe(
      () => {
        this.cookieService.delete('token', '/', 'localhost');
        this.userToken.next(null);
        this.router.navigateByUrl(this.translate.currentLang + '/home');
        this.socketService.unsubscribeFromNotifications(this.userService.loggedUser.value);
        this.userService.loggedUser.next(null);
        this.notificationService.notificationList.next(null);
        this.propertyService.propertyList.next(null);
        this.paymentsService.paymentList.next(null);
        this.router.navigateByUrl(this.translate.currentLang + '/home');
      },
      (e: any) => {
        this.toastr.error("If the error persists, try deleting your cookies.", "An error occured!", {timeOut: 7000});
      }
    );
  }

  changePassword(changePassword: ChangePassword){
    return this.requestService.post("users/change-pass", changePassword);
  }

}
