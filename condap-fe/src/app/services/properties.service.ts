import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PropertyDTO } from '../models/DTOs/property-dto.model';
import { Property } from '../models/property.model';
import { RequestService } from './request/request.service';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {

  propertyList: BehaviorSubject<Property[]> = new BehaviorSubject<Property[]>([]);
  fetching: boolean = false;

  constructor(private requestService: RequestService,
    private toastr: ToastrService) {}

  fetchProperties(){
    this.fetching = true;
    this.requestService.get("properties").subscribe(
      data => {
        this.propertyList.next(data as Property[]);
        this.fetching = false;
      },
      e => this.toastr.error("Error getting properties!")
    );
  }

  addProperty(propertyDto: PropertyDTO){
    return this.requestService.post("properties", propertyDto).toPromise()
    .then(data => {
      var properties: Property[] = this.propertyList.value;
      properties.push(data as Property);
      this.propertyList.next(properties);
      this.toastr.success("Property added!");
    })
  }

  updateProperty(propertyId: number, propertyDto: PropertyDTO){
    return this.requestService.put("properties/" + propertyId, propertyDto).toPromise()
    .then(data => {
      var properties: Property[] = this.propertyList.value;
      properties.forEach(property => {
        if(property.id == propertyId) property = data;
      })
      this.propertyList.next(properties);
      this.fetchProperties();
      this.toastr.success("Property updated!");
    })
  }

  deleteProperty(property: Property){
    this.requestService.delete("properties/", property.id).subscribe(data => {
      var properties: Property[] = this.propertyList.value.filter(obj => obj != property);
      this.propertyList.next(properties);
      this.toastr.success("Property deleted!");
    },
      e => this.toastr.error(e.error.message)
    );
  }

  selectPropertyFromList(propertyId: number){
    var properties = this.propertyList.value;
    return properties.find(x => x.id == propertyId);
  }

  get getProperties(){
    return this.propertyList.value;
  }
}
