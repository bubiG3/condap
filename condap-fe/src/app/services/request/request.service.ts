import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private apiBaseUrl = "http://localhost:8000/api/";

  constructor(protected httpClient: HttpClient) { 
    this.httpClient.get<any>("http://localhost:8000/sanctum/csrf-cookie", { withCredentials: true }).subscribe()
  }

  public post(endpoint: string, vm?: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    if (vm)
      return this.httpClient
        .post<any>(this.apiBaseUrl + endpoint, vm instanceof FormData ? vm : JSON.stringify(vm), vm instanceof FormData ? {} : { headers } );
    else
      return this.httpClient
        .post<any>(this.apiBaseUrl + endpoint, { headers });
  }

  public put(endpoint: string, vm: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.httpClient
      .put<any>(this.apiBaseUrl + endpoint, JSON.stringify(vm), { headers });
  }

  public delete(endpoint: string, id: number): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    // let params = new HttpParams().append('id', id.toString());

    return this.httpClient
      .delete<any>(this.apiBaseUrl + endpoint + id);
  }

  public get(endpoint: string
    // , queryParams?: NewType[]
    ): Observable<any> {

    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    // if (queryParams) {

    //   let params = new HttpParams();

    //   queryParams.forEach(qp => {
    //     params = params.append(qp.key, qp.value)
    //   });

    //   return this.httpClient
    //     .get<any>(this.apiBaseUrl + endpoint, { headers, withCredentials: true, params });
    // }
    // else {
      return this.httpClient
        .get<any>(this.apiBaseUrl + endpoint);
    // }
  }
}
