
    <style>
        .table {
            width: 90%
        }
        .border {
            border-top: solid 1px black;
            border-bottom: solid 1px black;
        }
        .font {
            font-family:sans-serif
        }
    </style>

    <div class='font'>
        <h1>
            Conjunto Residencial Monteverde
        </h1>
        <h2>
            Recibo de EXPENSAS
        </h2>
        <p>
            Fecha de emision: {{ $receiptData['currentDate'] }}
        </p>
        <p>
            Número de recibo: <b>gotta ask first</b>
        </p>

        <br />
        
        <p>
            Propiedad: {{ $receiptData['propertyParcelNumber'] . $receiptData['propertyBlock'] }}
        </p>
        <p>
            Recibo de: {{ $receiptData['userFullName'] }}
        </p>

        <br />

        <div class='container mt-7'>
            <table class='table table-bordered mb-7'>
                <tbody>
                    @foreach ($receiptData['paymentPeriod'] as $month)
                        <tr>
                            <td>{{ $month }}</td>
                            <td>${{ $receiptData['taxPerMonth'] }}</th>
                        </tr>
                    @endForeach
                </tbody>
                
                {{-- <tbody class='border'>
                    <tr>
                        <td scope='col'>Anterior</td>
                        <td scope='col'>- ${{ $receiptData['balanceBeforePayment'] }}</td>
                    </tr>
                    <tr>
                        <td scope='col'><strong>Final</strong></td>
                        <td scope='col'><strong>${{ $receiptData['balanceAfterPayment'] }}</strong></td>
                    </tr>
                </tbody> --}}

                <tbody class='border'>
                    <tr>
                        <td scope='col'>Total</td>
                        <td scope='col'>${{ $receiptData['amountPaid'] }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <br />

        <p>
            Emitido por: {{ $receiptData['adminFullName'] }}
        </p>
        <p>
            En caso de tener preguntas sobre este recibo contacte a la administración.
        </p>
    </div>