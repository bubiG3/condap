<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
*   PUBLIC routes
*/

/* Users */
Route::post('users/login', ['App\Http\Controllers\UserController', 'login']);

/*
*   PROTECTED routes
*/  


Route::group(['middleware'=>['auth:sanctum', 'cors']], function () {
    /* Roles - all admin */
    Route::get('user-roles', ['App\Http\Controllers\UserRoleController', 'index']);
    Route::get('user-roles/{id}', ['App\Http\Controllers\UserRoleController', 'show']);
    Route::post('user-roles', ['App\Http\Controllers\UserRoleController', 'store']);
    Route::put('user-roles/{id}', ['App\Http\Controllers\UserRoleController', 'update']);
    Route::delete('user-roles/{id}', ['App\Http\Controllers\UserRoleController', 'destroy']);

    /* Users */
    Route::get('users', ['App\Http\Controllers\UserController', 'index']); //for now open, later admin
    Route::get('users/{id}', ['App\Http\Controllers\UserController', 'show']); //for now open, later admin 
    Route::post('users', ['App\Http\Controllers\UserController', 'create']); //admin
    Route::post('users/logout', ['App\Http\Controllers\UserController', 'logout']);
    Route::put('users/{id}', ['App\Http\Controllers\UserController', 'update']); //admin, auth(name, dob, picture)
    Route::delete('users/{id}', ['App\Http\Controllers\UserController', 'destroy']); //admin
    Route::patch('users/picture', ['App\Http\Controllers\UserController', 'updatePicture']); //auth
    Route::get('me', ['App\Http\Controllers\UserController', 'getCurrentUser']);
    Route::patch('users/{id}/status', ['App\Http\Controllers\UserController', 'updateUserStatus']); //admin
    Route::post('users/change-pass', ['App\Http\Controllers\UserController', 'changePass']); //auth
    
    /* Receipts */
    Route::get('receipts', ['App\Http\Controllers\ReceiptController', 'index']); //admin, landl - for all properties, tenant - only theirs 
    //Route::get('receipts/{id}', ['App\Http\Controllers\ReceiptController', 'show']);
    //create - admin
    Route::put('receipts/{id}', ['App\Http\Controllers\ReceiptController', 'update']); //admin
    Route::delete('receipts/{id}', ['App\Http\Controllers\ReceiptController', 'destroy']); // admin

    /* Properties */
    Route::get('properties', ['App\Http\Controllers\PropertyController', 'index']); //admin, landlord
    //Route::get('properties/{id}', ['App\Http\Controllers\PropertyController', 'show']);
    Route::post('properties', ['App\Http\Controllers\PropertyController', 'store']); // admin
    Route::put('properties/{id}', ['App\Http\Controllers\PropertyController', 'update']); //admin
    Route::delete('properties/{id}', ['App\Http\Controllers\PropertyController', 'destroy']); //admin

    /* Payments */
    Route::get('payments', ['App\Http\Controllers\PaymentController', 'index']); //admin, landl - for all properties, tenant - only theirs 
    Route::get('payments/{id}', ['App\Http\Controllers\PaymentController', 'show']);
    Route::post('payments', ['App\Http\Controllers\PaymentController', 'store']);
    Route::put('payments/{id}', ['App\Http\Controllers\PaymentController', 'updateAfterRejected']);//all users
    Route::delete('payments/{id}', ['App\Http\Controllers\PaymentController', 'destroy']);//admin
    // special payment routes
    Route::patch('payments/deny/{id}', ['App\Http\Controllers\PaymentController', 'denyPayment']);//admin
    Route::patch('payments/approve/{id}', ['App\Http\Controllers\PaymentController', 'approvePaymentAndCreateReceipt']);//admin

    /* Notifications - DISCUSS PERMISSIONS */
    Route::get('notifications', ['App\Http\Controllers\NotificationController', 'getCurrentUserNotifications']); 
    Route::get('notifications/{id}', ['App\Http\Controllers\NotificationController', 'show']);
    Route::post('notifications', ['App\Http\Controllers\NotificationController', 'broadcastToAll']);
    Route::post('notifications/{id}', ['App\Http\Controllers\NotificationController', 'markOneAsSeen']);
    Route::post('notifications-see-all', ['App\Http\Controllers\NotificationController', 'markAllAsSeen']);
    Route::delete('notifications/{id}', ['App\Http\Controllers\NotificationController', 'destroy']);

    /* Norms - DISCUSS */
    Route::get('norms', ['App\Http\Controllers\NormController', 'index']); 
    Route::get('norms/{id}', ['App\Http\Controllers\NormController', 'show']);
    Route::post('norms', ['App\Http\Controllers\NormController', 'store']);
    Route::put('norms/{id}', ['App\Http\Controllers\NormController', 'update']);
    Route::delete('norms/{id}', ['App\Http\Controllers\NormController', 'destroy']);

    /* Property types - admin */
    Route::get('property-types', ['App\Http\Controllers\PropertyTypeController', 'index']); 
    Route::get('property-types/{id}', ['App\Http\Controllers\PropertyTypeController', 'show']);
    Route::post('property-types', ['App\Http\Controllers\PropertyTypeController', 'store']);
    Route::put('property-types/{id}', ['App\Http\Controllers\PropertyTypeController', 'update']);
    Route::delete('property-types/{id}', ['App\Http\Controllers\PropertyTypeController', 'destroy']);

    /* News */
    Route::post('news', ['App\Http\Controllers\NewsController', 'store']);
    Route::put('news/{id}', ['App\Http\Controllers\NewsController', 'update']);
    Route::delete('news/{id}', ['App\Http\Controllers\NewsController', 'destroy']);
    Route::get('news', ['App\Http\Controllers\NewsController', 'index']); 
    Route::get('news/{id}', ['App\Http\Controllers\NewsController', 'show']);
    Route::patch('news/{id}', ['App\Http\Controllers\NewsController', 'updateNewsPicture']);

    /* Feedback */
    Route::get('feedbacks', ['App\Http\Controllers\FeedbackController', 'index']); 
    Route::get('feedbacks/{id}', ['App\Http\Controllers\FeedbackController', 'show']);
    Route::post('feedbacks', ['App\Http\Controllers\FeedbackController', 'store']);
    Route::put('feedbacks/{id}', ['App\Http\Controllers\FeedbackController', 'update']);
    Route::delete('feedbacks/{id}', ['App\Http\Controllers\FeedbackController', 'destroy']);

    /* Files */
    Route::get('file/user/{filename}', ['App\Http\Controllers\FileController', 'profilePicture']);
    Route::get('file/news/{filename}', ['App\Http\Controllers\FileController', 'news']);
    Route::get('file/payment/{filename}', ['App\Http\Controllers\FileController', 'payment']);
    Route::get('file/receipt/{paymentId}', ['App\Http\Controllers\FileController', 'receipt']);
    Route::get('file/norm/{filename}', ['App\Http\Controllers\FileController', 'norm']);

});