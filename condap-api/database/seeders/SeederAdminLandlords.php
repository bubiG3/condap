<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class SeederAdminLandlords extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Special admin user
        User::create([
            'first_name' => 'Admin',
            'last_name' => 'the Admin',
            'dob' => '1990-01-01',
            'phone' => '0123456789',
            'ci' => '0123456789',
            'picture' => 'default_user.png',
            'role_id' => 1,
            'email' => 'admin@gmail.com',
            'password' => bcrypt('Condap_Administrat0r'),
            'active'=>true
        ]);
        // Landlord 1
        User::create([
            'first_name' => 'Landlord',
            'last_name' => 'First',
            'dob' => '1990-01-01',
            'phone' => '0123456789',
            'picture' => 'default_user.png',
            'role_id' => 2,
            'email' => 'landlord1@gmail.com',
            'password' => bcrypt('password'),
            'ci' => '0123456789',
            'active'=>true
        ]);
        // Landlord 2
        User::create([
            'first_name' => 'Landlord',
            'last_name' => 'Second',
            'dob' => '1990-01-01',
            'phone' => '0123456789',
            'picture' => 'default_user.png',
            'role_id' => 2,
            'email' => 'landlord2@gmail.com',
            'password' => bcrypt('password'),
            'ci' => '0123456789',
            'active'=>true
        ]);
    }
}
