<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Feedback;

class SeederFeedbacks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            Feedback::create([
                'content' => 'Content of feedback nr ' . $i,
                'user_id' => random_int(2,3),
                'created_at' => now(),
            ]);
        }
    }
}
