<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PropertyType;

class SeederPropertyTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PropertyType::create([
            'name' => 'apartment'
        ]);

        PropertyType::create([
            'name' => 'land'
        ]);

        PropertyType::create([
            'name' => 'uni-house'
        ]);

        PropertyType::create([
            'name' => 'multi-house'
        ]);
    }
}
