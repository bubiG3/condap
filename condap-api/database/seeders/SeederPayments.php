<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Payment;

class SeederPayments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Payment created to demonstrate late
        Payment::create([
            'property_id' => 1,
            'user_id' => 2,
            // 'picture' => 'default.png',
            'status_id' => 2,
            'start_date' => '2021-01-01',
            'end_date' => '2021-03-31'
        ]);
        
    }
}
