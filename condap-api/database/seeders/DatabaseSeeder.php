<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SeederUserRoles::class,
            SeederAdminLandlords::class,
            SeederPropertyTypes::class,
            //SeederNorms::class,
            SeederNotifications::class,
            // SeederNews::class,
            SeederProperties::class,
            SeederTenantsOccupants::class,
            // SeederFeedbacks::class,
            SeederPaymentStatuses::class,
            SeederPayments::class,
            // SeederReceipts::class,
        ]);
    }
}
