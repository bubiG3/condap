<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Receipt;

class SeederReceipts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            Receipt::create([
                'payment_id' => random_int(1,7),
                'picture' => 'default.png'
            ]);
        }
    }
}
