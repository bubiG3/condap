<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Norm;

class SeederNorms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            Norm::create([
                'title' => 'This is norms title nr ' . $i,
                'file_name' => 'Lorem ipsum etc...',
                'created_at' => now()
            ]);
        }
    }
}
