<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class SeederUserRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserRole::create([
            'name' => 'admin'
        ]);

        UserRole::create([
            'name' => 'landlord'
        ]);

        UserRole::create([
            'name' => 'tenant'
        ]);

        UserRole::create([
            'name' => 'occupant'
        ]);
    }
}
