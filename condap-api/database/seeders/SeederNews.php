<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\News;

class SeederNews extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            News::create([
                'title' => 'This is news title nr ' . $i,
                'content' => 'Lorem ipsum etc...',
                'picture' => null
            ]);
        }
    }
}
