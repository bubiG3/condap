<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Property;

class SeederTenantsOccupants extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Tenant',
            'last_name' => 'First',
            'dob' => '1990-01-01',
            'phone' => '0123456789',
            'picture' => 'default_user.png',
            'role_id' => 3,
            'email' => 'tenant1@gmail.com',
            'password' => bcrypt('password'),
            //'property_id' => 1,
            'ci' => '0123456789',
            'active'=>true
        ]);
        User::create([
            'first_name' => 'Tenant',
            'last_name' => 'Second',
            'dob' => '1990-01-01',
            'phone' => '0123456789',
            'picture' => 'default_user.png',
            'role_id' => 3,
            'email' => 'tenant2@gmail.com',
            'password' => bcrypt('password'),
            //'property_id' => 2,
            'ci' => '0123456789',
            'active'=>true
        ]);
        User::create([
            'first_name' => 'Occupant',
            'last_name' => 'First',
            'dob' => '1990-01-01',
            'phone' => '0123456789',
            'picture' => 'default_user.png',
            'role_id' => 4,
            'email' => 'occ1@gmail.com',
            'password' => bcrypt('password'),
            //'property_id' => 2,
            'ci' => '0123456789',
            'active'=>true
        ]);
        User::create([
            'first_name' => 'Occupant',
            'last_name' => 'Second',
            'dob' => '1990-01-01',
            'phone' => '0123456789',
            'picture' => 'default_user.png',
            'role_id' => 4,
            'email' => 'occ2@gmail.com',
            'password' => bcrypt('password'),
            //'property_id' => 2,
            'ci' => '0123456789',
            'active'=>true
        ]);

        $property1 = Property::findOrFail(1);
        $property2 = Property::findOrFail(2);
        $property3 = Property::findOrFail(3);
        $property4 = Property::findOrFail(4);
        $start_date = now();
        $end_date = null;

        User::find(2)->properties()->save($property1, array('start_date' => $start_date, 'end_date' => $end_date));
        User::find(2)->properties()->save($property2, array('start_date' => $start_date, 'end_date' => $end_date));
        User::find(3)->properties()->save($property3, array('start_date' => $start_date, 'end_date' => $end_date));
        User::find(3)->properties()->save($property4, array('start_date' => $start_date, 'end_date' => $end_date));

        User::find(4)->property()->save($property1, array('start_date' => $start_date, 'end_date' => $end_date));
        User::find(5)->property()->save($property2, array('start_date' => $start_date, 'end_date' => $end_date));

        User::find(6)->property()->save($property1, array('start_date' => $start_date, 'end_date' => $end_date));
        User::find(7)->property()->save($property1, array('start_date' => $start_date, 'end_date' => $end_date));
    }
}
