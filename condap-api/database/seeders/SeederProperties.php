<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Property;

class SeederProperties extends Seeder
{
    /**
     * NOTE: NEVER EVER TOUCH THESE! Those are the actual properties. Thanks. :)
     *
     * @return void
     */
    public function run()
    {

        //houses

        Property::create([
            'parcel_number' => 1,
            'block' => 'A',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 2,
            'block' => 'A',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 3,
            'block' => 'A',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 4,
            'block' => 'A',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 1,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 2,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 3,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 4,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 5,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 6,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 7,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 8,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 9,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 10,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 11,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 13,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 14,
            'block' => 'B',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 102,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 1,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 2,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 3,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 4,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 5,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 6,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 7,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 8,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 9,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 10,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 11,
            'block' => 'D',
            'type_id' => 3,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => 'D',
            'type_id' => 3,
        ]);
        
        //appartments

        Property::create([
            'parcel_number' => 11,
            'block' => '1',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => '1',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 21,
            'block' => '1',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 22,
            'block' => '1',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 31,
            'block' => '1',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 32,
            'block' => '1',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 11,
            'block' => '2',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => '2',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 21,
            'block' => '2',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 22,
            'block' => '2',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 31,
            'block' => '2',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 32,
            'block' => '2',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 11,
            'block' => '3',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => '3',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 21,
            'block' => '3',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 22,
            'block' => '3',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 31,
            'block' => '3',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 32,
            'block' => '3',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 11,
            'block' => '4',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => '4',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 21,
            'block' => '4',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 22,
            'block' => '4',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 31,
            'block' => '4',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 32,
            'block' => '4',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 11,
            'block' => '5',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => '5',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 21,
            'block' => '5',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 22,
            'block' => '5',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 31,
            'block' => '5',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 32,
            'block' => '5',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 11,
            'block' => '6',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 12,
            'block' => '6',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 21,
            'block' => '6',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 22,
            'block' => '6',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 31,
            'block' => '6',
            'type_id' => 1,
        ]);
        Property::create([
            'parcel_number' => 32,
            'block' => '6',
            'type_id' => 1,
        ]);
    }
}