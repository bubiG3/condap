<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PropertyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('property_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('property_user_property_id_foreign');
        $table->dropColumn('property_id');
        $table->dropForeign('property_user_user_id_foreign');
        $table->dropColumn('user_id');
        Schema::dropIfExists('property_user');
    }
}
