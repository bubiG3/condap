<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->integer('parcel_number');
            $table->string('block');
            $table->bigInteger('type_id')->unsigned();
            $table->timestamps();

            //constraints
            $table->foreign('type_id')->references('id')->on('property_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('properties_type_id_foreign');
        $table->dropColumn('type_id');
        Schema::dropIfExists('properties');
    }
}
