<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NotificationUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('notification_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->boolean('seen_state');

            $table->foreign('notification_id')->references('id')->on('notifications')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('notification_user_notification_id_foreign');
        $table->dropColumn('notification_id');
        $table->dropForeign('notification_user_user_id_foreign');
        $table->dropColumn('user_id');
        $table->dropColumn('seen_state');

        Schema::dropIfExists('notification_user');
    }
    
}
