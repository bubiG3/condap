<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPropertyFkToUsers extends Migration
{
    /**
    //  * Run the migrations.
    //  *
    //  * @return void
    //  */
    // public function up()
    // {
    //     Schema::table('users', function($table) {
    //         $table->bigInteger('property_id')->nullable()->unsigned(); //tenants get assigned a property on create

    //         $table->foreign('property_id')->references('id')->on('properties')->onDelete('SET NULL');
    //     });
    // }

    // /**
    //  * Reverse the migrations.
    //  *
    //  * @return void
    //  */
    // public function down()
    // {
    //     Schema::table('users', function($table) {
    //         $table->dropColumn('property_id');
    //     });
    // }
}
