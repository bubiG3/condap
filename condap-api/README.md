# Table of contents
[Introduction to Condap](https://git.fhict.nl/I401075/condap-api/-/tree/develop#introduction-to-condap)  
[Installation guide](https://git.fhict.nl/I401075/condap-api/-/tree/develop/README.md#installation-guide-for-backend-application)  
[Seeded credentials](https://git.fhict.nl/I401075/condap-api/-/tree/develop/README.md#seeded-credentials)  
[Technicalities](https://git.fhict.nl/I401075/condap-api/-/tree/develop/README.md#technicalities)  
[Requirements coverage](https://git.fhict.nl/I401075/condap-api/-/tree/develop/README.md#requirements-coverage)   

## Introduction to _Condap_
_Condap_ is a system which allows you to manage everything about your condominium. Why should you choose this app?  

**Simplistic & beautiful design**  
We have taken into consideration that administrative work can be really stressful. Therefore, _Condap_'s has been designed with care and thought for the user.

**Responsive**  
In the era of smartphones, no website is complete without a mobile view. _Condap_ can also be used on devices with different screen size.

**Convenient for every user**  
Are you a resident of the condominium? Or maybe you own properties here? Contact the administration and log in into _Condap_ with your account!

## Installation guide for backend application  
After running Apache and MySQL in XAMPP, follow the steps:  
**To install composer** run command `composer install`  
**To set up environment** from the root folder copy _.env.example_ and rename the copy to _.env_  
**To generate an application key** run command `php artisan key:generate`  
**To create a link with public storage** run command `php artisan storage:link`  
**To create database tables & seed it with some initial information** run command `php artisan migrate:fresh --seed`  
**To serve the application** run command `php artisan serve`  
**To enable notifications** run command `php artisan websockets:serve`  
**To be able to open PDFs** download extention _Allow CORS: Access-Control-Allow-Origin_ by Muyor  

To get the WEB application running, go to its [repository](https://git.fhict.nl/I401075/condap-fe).

## Seeded credentials

#### Admin
- email: admin@gmail.com
- password: Condap_Administrat0r

#### Landlord
- email: landlord1@gmail.com
- password: password

#### Tenant
- email: tenant1@gmail.com
- password: password

## Technicalities

### C4 Model
_**Fig.1.** C4: Context_  
<a href="https://ibb.co/jySJSPy"><img src="https://i.ibb.co/fksDszk/c4web-Context.png" alt="c4web-Context" border="0"></a>  
_**Fig.2.** C4: Containers_   
<a href="https://ibb.co/jJV30NF"><img src="https://i.ibb.co/8YzPvCV/c4web-Containers.png" alt="c4web-Containers" border="0"></a>  
_**Fig.3.** C4: Components_  
<a href="https://ibb.co/GRsVMm6"><img src="https://i.ibb.co/cQbN8qB/c4web-Components.png" alt="c4web-Components" border="0"></a>  

### Browser support

| Chrome  | Mozilla | Edge |
| ------- | ------- | ---- |
| Supported | Supported | Supported |

## Requirements coverage

[Go To Condap FE](https://git.fhict.nl/I401075/condap-fe)
