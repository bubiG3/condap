<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Payment;
use App\Models\PropertyType;
use Illuminate\Support\Facades\DB;

class Property extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parcel_number',
        'block',
        'type_id',
        'landlord_id'
    ];

    protected $hidden = [
        'landlord_id',
        'type_id',
        'pivot'
    ];

    protected $appends = ['property_number'];

    // public function getTenantsAttribute(){
    //     $tenants = $this->tenants()
    //     ->wherePivot('end_date', null)
    //     ->where('role_id', 3 )
    //     ->get();

    //     $idsArray = [];

    //     foreach($tenants as $tenant){
    //         array_push($idsArray, $tenant->id);
    //     }

    //     return $idsArray;
    // }

    // public function getOccupantsAttribute(){
    //     $occupants = $this->occupants()
    //     ->wherePivot('end_date', null)
    //     ->where('role_id', 4 )
    //     ->get();

    //     $idsArray = [];

    //     foreach($occupants as $occupant){
    //         array_push($idsArray, $occupant->id);
    //     }

    //     return $idsArray;
    // }

    // public function getLandlordAttribute(){
    //     $landlord = $this->landlord()
    //     ->wherePivot('end_date', null)
    //     ->where('role_id', 2)
    //     ->first();

    //     return $landlord ? $landlord->id : null;
    // }

    // public function getLandlordAttribute(){
    //     $landlord = $this->landlord()
    //     ->wherePivot('end_date', null)
    //     ->where('role_id', 2)
    //     ->first();
    //     return $landlord;
    // }

    public function getPropertyNumberAttribute(){
        $propertyNumber = $this->parcel_number . $this->block;
        return $propertyNumber;
    }

    public function getTypeAttribute(){
        $type = $this->type();
        return $type;
    }

    public function landlord()
    {
        return $this->belongsToMany(User::class)
        ->withPivot('start_date', 'end_date')
        ->wherePivot('end_date', null)
        ->where('role_id', 2 );
    }

    public function tenants()
    {
        return $this->belongsToMany(User::class)
        ->withPivot('start_date', 'end_date')
        ->wherePivot('end_date', null)
        ->where('role_id', 3 );
    }

    public function occupants(){
        return $this->belongsToMany(User::class)
        ->withPivot('start_date', 'end_date')
        ->wherePivot('end_date', null)
        ->where('role_id', 4 );
    }

    public function type()
    {
        return $this->belongsTo(PropertyType::class);
        //PropertyType::where('id', $this->type_id)->first();
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'property_id')->where('status_id', 2);
    }

    //custom query for status to be filled
    //should check if payment for the current month exists
    //if yes -> property status = payment status
    //if no -> property status = "NO PAYMENT" or sth like that
    
    use HasFactory;
}
