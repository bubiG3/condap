<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Payment;

class Image extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'payment_id'
    ];

    protected $hidden = [
        'payment_id', 'created_at', 'updated_at', 'id'
    ];

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    use HasFactory;
}
