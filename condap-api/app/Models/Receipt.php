<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Payment;

class Receipt extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'receipts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_id',
        'picture',
    ];

    protected $hidden = [
        'payment_id'
    ];

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }
    
    use HasFactory;
}
