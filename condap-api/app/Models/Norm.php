<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Norm extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'norms';
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'file',
        'file_name',
        'created_at'
    ];

    use HasFactory;
}
