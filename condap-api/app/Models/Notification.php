<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Notification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'target_roles',
    ];

    protected $appends = ['seen_state'];

    protected $hidden = ['pivot'];

    public function users()
    {
        return $this->belongsToMany(User::class)
        ->withPivot('seen_state');
    }

    public function seen_state()
    {
        return $aaa = $this->users()->first()->pivot->seen_state;
    }

    public function getSeenStateAttribute(){
        $state = $this->seen_state();
        return $state;
    }
    
    use HasFactory;
}
