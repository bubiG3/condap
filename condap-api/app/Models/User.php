<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

use Laravel\Sanctum\HasApiTokens;

use App\Models\UserRole;
use App\Models\Property;
use App\Models\Notification;
use App\Models\Payment;

class User extends Authenticatable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'role_id',
        'property_id',
        'pivot'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'ci',
        'dob',
        'phone',
        'picture',  
        'role_id',
        'email',
        'password',
        'property_id',
        'active'
    ];

    //protected $appends = ['role'];

    // public function getPropertyAttribute(){
    //     $a = $this->property()
    //     ->wherePivot('end_date', null)
    //     ->first();

    //     return $a->id;
    // }

    // public function getPropertiesAttribute(){
    //     $a = $this->properties()
    //     ->wherePivot('end_date', null)
    //     ->get();

    //     $idsArray = [];

    //     foreach($a as $aa){
    //         array_push($idsArray, $aa->id);
    //     }

    //     return $idsArray;
    // }

    // public function getRoleAttribute(){
    //     $role = $this->role();
    //     return $role;
    // }

    public function role()
    {
        return $this->belongsTo(UserRole::class);
    }

    public function property() //check if user is active
    {
        return $this->belongsToMany(Property::class)
        ->withPivot('start_date', 'end_date')
        ->wherePivot('end_date', null);
    }

    public function properties() // check if user is active
    {
        return $this->belongsToMany(Property::class)
        ->withPivot('start_date', 'end_date')
        ->wherePivot('end_date', null);
    }

    public function notifications()
    {
        return $this->belongsToMany(Notification::class)
        ->withPivot('seen_state');
    }

    public function payments()
    {
        return $this->belongsToMany(Payment::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     *
     *   protected $casts = [
     *      'email_verified_at' => 'datetime',
     *   ];
     */
    
    use HasFactory, Notifiable, HasApiTokens, SoftDeletes;
}
