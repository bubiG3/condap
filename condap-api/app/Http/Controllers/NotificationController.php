<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\User;
use App\Events\TestEvent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class NotificationController extends Controller
{
    public function index()
    {
        return Notification::all();
    }
 
    public function show($id)
    {
        return Notification::find($id);
    }

    public function update(Request $request, $id)
    {
        $notification = Notification::findOrFail($id);
        $notification->update($request->all());

        return response()->json($notification);
    }

    // TODO but for later: updating the whole list of notifications as seen

    public function getCurrentUserNotifications(Request $request)
    {
        return Auth::user()->notifications()->get()->reverse()->values();
    }

    /**
     * Mark single notification as "seen".
     */
    public function markOneAsSeen($id) {

        $user = User::findOrFail(Auth::id());

        if (! Gate::allows('canUpdate', $user)) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }

        $notification = Notification::findOrFail($id);

        $notificationOfUser = $user->notifications()->wherePivot('notification_id', $notification->id)->first();
        if(isset($notificationOfUser)){
            $notification->users()->wherePivot('notification_id', $notification->id )->update(array(
                'seen_state' => 1,
            ));
        }
        
        return response()->json($notificationOfUser);
    }

     /**
     * Mark ALL notifications as "seen".
     */
    public function markAllAsSeen() {

        $user = User::findOrFail(Auth::id());

        if (! Gate::allows('canUpdate', $user)) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }

        $notificationsOfUser = $user->notifications()->wherePivot('user_id', $user->id)->where('seen_state', false)->get();
        if(count($notificationsOfUser) > 0){
            
            foreach($notificationsOfUser as $notification){
                $notification->users()->wherePivot('notification_id', $notification->id)->update(array(
                    'seen_state' => 1,
                ));
            }
        }
    
        return response()->json($user->notifications()->wherePivot('user_id', $user->id)->get()->reverse()->values());
    }


    /**
     * Broadcast to specified roles (role ids come in as an array). To broadcast to all users, select all roles.
     */
    public function broadcastToAll(Request $request) {
        if (! Gate::allows('isAdmin')) {
            return response()->json(['message'=>'Unauthorized.'], 401);
        }

        $validatedFields = $request->validate([
            'title' => 'required|string|max:2550',
            'content' => 'required|string|max:2550',
            'target_roles' => 'required'
        ]);

        global $createdNotification;
        $createdNotification = Notification::create([
            'title' => $validatedFields['title'],
            'content' => $validatedFields['content'],
            'target_roles' => $validatedFields['target_roles'], //might have to encode
            'created_at' => now()
        ]);

        //extract all role id's
        $userRoleIdsToBroadcastTo = json_decode($validatedFields['target_roles']);
        //for each role, for each user create an association with a notification
        foreach ($userRoleIdsToBroadcastTo as $userRoleId) {
            User::where('role_id', $userRoleId)
            ->get()
            ->each(function ($user) {
                $GLOBALS['createdNotification']->users()->save($user, array('seen_state' => false));
            });
        }

        broadcast(new TestEvent($createdNotification));

        return response()->json(['message'=>'Message sent.'], 200);
    }

    public function destroy(Request $request, $id)
    {
        $notification = Notification::findOrFail($id);
        $notification->destroy();

        return 204;
    }
}
