<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\Property;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Admin can get all users, with all their info.
     * Landlord gets all data of all users asociated to them - tenants & occupants of their properties.
     * Tenants & occupants can see only themselves and non-sensitive data of their landlord.
     */
    public function index()
    {
        $currentUser = Auth::user();

        if (! Gate::allows('isAdmin')) {

            if(Gate::allows('isLandlord')){
                $currentUserProperties = $currentUser->properties;
                $tenantsAndOccupants=[];

                foreach($currentUserProperties as $property){

                    $tenants = $property->tenants()->get();
                    $occupants = $property->occupants()->get();

                    foreach($tenants as $tenant){
                        array_push($tenantsAndOccupants, $tenant->load('role', 'property'));
                    }
                    foreach($occupants as $occupant){
                        array_push($tenantsAndOccupants, $occupant->load('role', 'property'));
                    }
                }
                return response()->json($tenantsAndOccupants, 200);
            }
            else{//tenant/occupant
                $currentUser->load('role');
                $currentUser->load('property');
                $currentUsersLandlord = [];
                foreach ($currentUser->property as $userProperty) {
                    if(count($userProperty->load('landlord')->landlord) > 0){
                        $currentUsersLandlord = User::select('first_name', 'last_name', 'email', 'phone', 'picture', 'role_id', 'active')
                        ->where('id', $userProperty->landlord[0]->id)->get();
                        if(count($currentUsersLandlord) > 0){
                            $currentUsersLandlord[0]->load('role');
                        }
                    }
                    
                }
                return response()->json($currentUsersLandlord, 200);
            }
        }

        $users = User::where('id', '!=', $currentUser->id)->get()->each(function ($user) {
            $user->load('role');
            if ($user->role_id == 2) {
                $user->load('properties');
            }
            else if ($user->role_id == 3 || $user->role_id == 4){
                $user->load('property');
            }  
        });
        return response()->json($users, 200);
    }
 
    /**
     * TODO:
     * Admin get all users, with all their info.
     * Landlord gets all users asociated to them - tenants & occupants of their properties.
     * Tenants & occupants can see only themselves and their landlord.
     */
    public function show($id)
    {
        $user = User::find($id);
        $user->load('role');
        if ($user->role_id == 2) {
            $user->load('properties');
        }
        else if ($user->role_id == 3 || $user->role_id == 4){
            $user->load('property');
        }  
        return response()->json($user);
    }

    public function update(Request $request, $id)
    {
        global $userToUpdate;
        $userToUpdate = User::findOrFail($id);
        
        if (! Gate::allows('canUpdate', $userToUpdate)) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }

        $userToUpdate->update($request->except('picture', 'property_id', 'role_id'));
        $property_id = $request['property_id'];
        
        if (! Gate::allows('isAdmin')) {
            return response()->json($userToUpdate, 200);
        }

        // If we are changing the role of the user, we want to remove their current relationships.
        if($userToUpdate->role_id != $request->role_id) {
           
            if($userToUpdate->role_id == 2) {
                $userToUpdate->properties()->wherePivot('end_date', null)->get()->each(function ($property) {
                    $GLOBALS['userToUpdate']->properties()->wherePivot('end_date', null)->updateExistingPivot($property->id, array(
                        'end_date' => now(),
                    ));
                });
            }
            else if($userToUpdate->role_id != 1) {
                $currentProperty = $userToUpdate->property()->wherePivot('end_date', null)->first();
                
                if(isset($currentProperty)){
                    $userToUpdate->property()->wherePivot('end_date', null)->updateExistingPivot($currentProperty->id, array(
                        'end_date' => now(),
                    ));
                }
            }
        }

        $userToUpdate->update(['role_id' => $request->role_id]);
        $userToUpdate = User::findOrFail($id);
        $userToUpdate->load('role');

        if ($userToUpdate->role_id == 2) {
            if(isset($property_id)){
                $property = Property::findOrFail($property_id);
                $start_date = now();
                $end_date = null;
                $userToUpdate->properties()->save($property, array('start_date' => $start_date, 'end_date' => $end_date));
            }
            $userToUpdate->load('properties');
        }
        
        else if ($userToUpdate->role_id != 1){
            if(isset($property_id)){
                $newProperty = Property::findOrFail($property_id);
                $currentProperty = $userToUpdate->property()->wherePivot('end_date', null)->first(); // TODO: remove "wherePivot('end_date', null)"

                if($newProperty->type_id == 3 && count($newProperty->tenants) > 0 && $userToUpdate->role_id == 3){
                    return response()->json([
                        'message' => 'Cannot add more tenants to property of type uni-house.'
                    ], 403);
                }

                if(isset($currentProperty)){
                    $userToUpdate->property()->wherePivot('end_date', null)->updateExistingPivot($currentProperty->id, array(
                        'end_date' => now(),
                    ));
                }
                $start_date = now();
                $end_date = null;
                $userToUpdate->property()->save($newProperty, array('start_date' => $start_date, 'end_date' => $end_date));
            }
            $userToUpdate->load('property');
        } 

        return response()->json($userToUpdate);
    }

    public function changePass(Request $request) {
        if(isset($request->old_password) && isset($request->new_password)){
            if(Hash::check($request->old_password, Auth::user()->password, [])) {
                Auth::user()->update(['password' => bcrypt($request->new_password)]);
                return response()->json([
                    'message'=>'Password updated successfully.',
                    'response_code' => 200], 200);
            }
            else {
                return response()->json([
                    'message'=>'Wrong current password provided.',
                    'response_code' => 400], 400);
            } 
        }
        return response()->json(['message'=>'Fields cannot be empty.'], 418);
    }

    public function destroy(Request $request, $id)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }
        User::destroy($id);
        return response()->json(['message'=>'User successfully deleted.'], 200);
    }

    public function create(Request $request)
    {
        // $loggedUser = Auth::user();

        // if(!isset($loggedUser)) {
        //     return response()->json([
        //         'message' => 'Unauthenticated.'
        //     ], 401);
        // }

        if (! Gate::allows('isAdmin')) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }

        $validatedFields = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'ci' => 'required',
            'dob' => 'required|date_format:Y-m-d',
            'phone' => 'required',
            'role_id' => 'required|integer',
            'email' => 'required|email|unique:users,email',
            'property_id' => 'integer'
        ]);

        $user = User::create([
            'first_name' => $validatedFields['first_name'],
            'last_name' => $validatedFields['last_name'],
            'ci' => $validatedFields['ci'],
            'dob' => $validatedFields['dob'] . ' 00:00:00',
            'phone' => $validatedFields['phone'],
            'picture' => $this->getFileNameAndSavePic($request, 'default_user.png'),
            'role_id' => $validatedFields['role_id'],
            'email' => $validatedFields['email'],
            'password' => bcrypt('password'),
            'active' => true,
        ]);
        
        $property_id = $request->has('property_id') ? $validatedFields['property_id'] : null;
        $user->load('role');

        if ($user->role_id == 2) {
            if(isset($property_id)){
                $property = Property::findOrFail($validatedFields['property_id']);
                $start_date = now();
                $end_date = null;
                $user->properties()->save($property, array('start_date' => $start_date, 'end_date' => $end_date));
            }
            $user->load('properties');
        }
        else if ($user->role_id != 1){
            if(isset($property_id)){
                $property = Property::findOrFail($validatedFields['property_id']);

                if($property->type_id == 3 && count($property->tenants) > 0 && $user->role_id == 3){
                    $user->forceDelete();
                    return response()->json([
                        'message' => 'Cannot add more tenants to property of type uni-house.'
                    ], 403);
                }

                $start_date = now();
                $end_date = null;
                $user->property()->save($property, array('start_date' => $start_date, 'end_date' => $end_date));
            }
            $user->load('property');
        } 

        return response()->json([
            'user' => $user
        ], 201);
    }

    public function login(Request $request, $remember = true)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            Auth::login($user, $remember);
            $token = $user->createToken('token')->plainTextToken;
            $user->load('role');

            if ($user->role_id == 2) {
                $user->load('properties');
            }
            else if ($user->role_id == 3 || $user->role_id == 4){
                $user->load('property');
            } 
            return response()->json([
                'user' => $user,
                'accessToken' => $token
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'Incorrect credentials.'
            ], 401);
        }
    }

    public function logout(Request $request)
    {
        $user = Auth::user();

        if ($user){
            Auth::guard('web')->logout();
            auth()->user()->tokens()->delete();
            return response()->json([
                'message' => 'Logged out successfully.'
            ], 200);

        } else { //i think this should be removed
            return response()->json([
                'message' => 'Unauthenticated.'
            ], 401);
        }
    }

    public function updatePicture(Request $request){
        $user = Auth::user();

        if (! Gate::allows('canUpdate', $user)) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }

        $profilePictureName = $this->getFileNameAndSavePic($request, $user['picture']);
        $user->update(['picture' => $profilePictureName]);
        $user->load('role');

        if ($user->role_id == 2) {
            $user->load('properties');
        }
        else if ($user->role_id == 3 || $user->role_id == 4){
            $user->load('property');
        } 
        return response()->json($user);
    }

    public function updateUserStatus($id){
        if (! Gate::allows('isAdmin')) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }

        $user = User::findOrFail($id);
        $user->update(['active' => !$user->active]);
        $user->load('role');

        if ($user->role_id == 2) {
            if($user->active == false){
                $currentProperties = $user->properties()->wherePivot('end_date', null)->allRelatedIds();
                $user->properties()->updateExistingPivot($currentProperties, array(
                    'end_date' => now(),
                ));
            }
            $user->load('properties');
        }
        else if ($user->role_id == 3 || $user->role_id == 4){
            if($user->active == false){
                $currentProperty = $user->property()->wherePivot('end_date', null)->first();
                if(isset($currentProperty)){
                    $user->property()->wherePivot('end_date', null)->updateExistingPivot($currentProperty->id, array(
                        'end_date' => now(),
                    ));
                }
            }
            $user->load('property');
        }
        return response()->json($user);
    }

    public function getCurrentUser(Request $request) {
        $user = Auth::user();
        $user->load('role');

        if ($user->role_id == 2) {
            $user->load('properties');
            global $lastPaymentForEachProperty;
            $lastPaymentForEachProperty=[];
            
            $properties = $user->properties()->get();
            if(count($properties) > 0){
                $properties->each(function ($property) {
                    $lastPaymentForThisProperty = $property->payments()->orderByDesc('end_date')->first();
                    if(isset($lastPaymentForThisProperty)){
                       array_push($GLOBALS['lastPaymentForEachProperty'], $lastPaymentForThisProperty->end_date); 
                    }
                });
    
                asort($lastPaymentForEachProperty);
                
                if(count($lastPaymentForEachProperty) > 0) {
                    $earliestPayment = $lastPaymentForEachProperty[0];
                    $user['last_payment_date'] = $earliestPayment;
                }
                
            }
        }

        else if ($user->role_id == 3 || $user->role_id == 4){
            $user->load('property');

            $lastPayment = $user->property()->first()
            ->payments()->orderByDesc('end_date')->first();

            if(isset($lastPayment)){
                $user['last_payment_date'] = $lastPayment->end_date;
            }
        }
        
        return response()->json($user);
    }

    public function getFileNameAndSavePic($request, $oldPictureName){
        if($request->hasFile('picture')){
            $validatedPicture = $request->validate([
                'picture' => 'mimes:jpg,jpeg,png,bmp,tiff|max:10000'
            ]);

            if(isset($oldPictureName)) {
                if($oldPictureName!='default_user.png'){
                    Storage::delete('public/profile_pictures/'.$oldPictureName);
                }
            }
            // set a random name to the picture, casting it to a string
            $fileName = (string) Str::uuid();
            $originalFileExtension = $request->file('picture')->getClientOriginalExtension();
            $fullFileName = $fileName.'.'.$originalFileExtension;
            // and save it to storage
            $path = $request->file('picture')->storeAs('public/profile_pictures', $fullFileName);
            return $fullFileName;
        }
        // else set filename to that of the default pic
        return $request->isMethod('patch') ? abort(406, 'A file must be attached.') : 'default_user.png';
    }
}
