<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Norm;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class NormController extends Controller
{
    public function index()
    {
        return Norm::all()->reverse()->values();
    }
 
    public function show($id)
    {
        return Norm::find($id);
    }

    public function store(Request $request)
    {
        if (! Gate::allows('isAdmin', Auth::user())) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }

        $validatedFields = $request->validate([
            'title' => 'required|string',
            'file' => 'required'
        ]);

        $fileName = $this->saveFileAndReturnFileName($request, null);
        $fields = [
            'file_name'=>$fileName,
            'title'=>$request->title,
            
            'created_at'=>now()
        ];
        return Norm::create($fields);
    }

    public function saveFileAndReturnFileName($request, $oldFileName) {
        if($request->has('file')){
            $allowedFileExtension=array('pdf');
            $file = $request->file('file');
            $fileOriginalExtension = $file->getClientOriginalExtension();
            $fileName = (string) Str::uuid();

            if(!in_array($fileOriginalExtension, $allowedFileExtension)){
                $readableExtensions = implode(', ', $allowedFileExtension);
                return abort(406, 'File type not supported. Allowed file types: '.$readableExtensions);
            }

            $file->storeAs('public/norms', $fileName);

            if(isset($oldFileName)){
                Storage::delete('public/norms/'.$oldFileName);
            }

            return $fileName;
        }
        return abort(406, 'A file must be attached.');
    }

    public function update(Request $request, $id)
    {
        if (! Gate::allows('isAdmin', Auth::user())) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }

        $validatedFields = $request->validate([
            'title' => 'required|string',
            'file' => 'required'
        ]);

        $norm = Norm::findOrFail($id);
        $newFileName = $this->saveFileAndReturnFileName($request, $norm->file_name);

        $fields = [
            'title'=>$request->title,
            'file_name'=>$newFileName
        ];

        $norm->update($fields);
        return $norm;
    }

    public function destroy(Request $request, $id)
    {
        if (! Gate::allows('isAdmin', Auth::user())) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }
        //delete the files as well!
        Norm::destroy($id);

        return 204;
    }
}
