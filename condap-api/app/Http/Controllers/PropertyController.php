<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use DateTime;

use App\Models\Property;
use App\Models\User;

class PropertyController extends Controller
{
    /**
     * Admin get all properties, with all their info attached.
     * Landlord gets all their proeprties asociated to them.
     * Tenants & occupants can see only their property.
     */
    public function index()
    {
        if (! Gate::allows('isAdmin')) {
            $currentUser = Auth::user();
            if(Gate::allows('isLandlord')){
                $currentUserProperties = $currentUser->properties()->get()->load('type', 'tenants', 'occupants');
                
                if(count($currentUserProperties) > 0){
                    $currentUserProperties->each(function ($property) {
                        if(count($property->payments()->get()) > 0){
                            $lastPaymentForThisProperty = $property->payments()->orderByDesc('end_date')->first();
                            if(isset($lastPaymentForThisProperty)){
                                $property['last_payment_date'] = $lastPaymentForThisProperty->end_date;
                                if(new DateTime($lastPaymentForThisProperty->end_date) < new DateTime()){
                                    $property['payment_status'] = "behind";
                                }
                                else{
                                    $property['payment_status'] = "clear";
                                }
                            }
                        }
                        else {
                            $property['last_payment_date'] = (new DateTime($property->created_at))->modify('last day of this month')->format("Y-m-d");
                            $property['payment_status'] = "new";
                        }
                    });
                }

                return response()->json($currentUserProperties, 200);
            }
            else{//tenant/occupant
                $currentUserProperties = $currentUser->properties()
                ->with([
                    'landlord' => function($query) {
                        $query->select('first_name', 'last_name', 'email', 'phone', 'picture', 'role_id', 'active');
                    }
                ])->get()->load('type');

                if(count($currentUserProperties) > 0){
                    $currentUserProperties->each(function ($property) {
                        if(count($property->payments()->get()) > 0){
                            $lastPaymentForThisProperty = $property->payments()->orderByDesc('end_date')->first();
                            if(isset($lastPaymentForThisProperty)){
                                $property['last_payment_date'] = $lastPaymentForThisProperty->end_date;
                                if(new DateTime($lastPaymentForThisProperty->end_date) < new DateTime()){
                                    $property['payment_status'] = "behind";
                                }
                                else{
                                    $property['payment_status'] = "clear";
                                }
                            }
                        }
                        else {
                            $property['last_payment_date'] = (new DateTime($property->created_at))->modify('last day of this month')->format("Y-m-d");
                            $property['payment_status'] = "new";
                        }
                    });
                }

                return response()->json($currentUserProperties, 200);
            }
        }
        $allProperties = Property::get()->load('tenants', 'landlord', 'occupants', 'type');
        if(count($allProperties) > 0){
            $allProperties->each(function ($property) {
                if(count($property->payments()->get()) > 0){
                    $lastPaymentForThisProperty = $property->payments()->orderByDesc('end_date')->first();
                    if(isset($lastPaymentForThisProperty)){
                        $property['last_payment_date'] = $lastPaymentForThisProperty->end_date;
                        if(new DateTime($lastPaymentForThisProperty->end_date) < new DateTime()){
                            $property['payment_status'] = "behind";
                        }
                        else{
                            $property['payment_status'] = "clear";
                        }
                    }
                }
                else {
                    //new
                    $property['last_payment_date'] = (new DateTime($property->created_at))->modify('last day of this month')->format("Y-m-d");
                    $property['payment_status'] = "new";
                }
            });
        }

        return response()->json($allProperties, 200);
    }
 
    /**
     * TODO:
     * Admin get all properties, with all their info attached.
     * Landlord gets all their proeprties asociated to them.
     * Tenants & occupants can see only their property.
     */
    public function show($id)
    {
        return response()->json(Property::find($id)->load('tenants', 'landlord', 'occupants', 'type'));
    }

    public function store(Request $request)
    {// for later https://stackoverflow.com/questions/54750907/how-to-validate-unique-combination-of-two-fields-in-laravel
        if (! Gate::allows('isAdmin')) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }

        $validatedFields = $request->validate([
            'parcel_number' => 'required|integer',
            'block' => 'required|string|max:255',
            'type_id' => 'required|integer',
            'landlord_id' => 'required|integer'
        ]);

        $property = Property::create([
            'parcel_number' => $validatedFields['parcel_number'],
            'block' => $validatedFields['block'],
            'type_id' => $validatedFields['type_id'],
        ]);

        $property['last_payment_date'] = (new DateTime())->modify('last day of this month')->format("Y-m-d");
        $landlord = User::findOrFail($validatedFields['landlord_id']);
        $property['payment_status'] = "new";

        $start_date = now();
        $end_date = null;

        $property->landlord()->save($landlord, array('start_date' => $start_date, 'end_date' => $end_date));

        return $property->load('tenants', 'landlord', 'occupants', 'type');
    }

    public function update(Request $request, $id)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }

        $property = Property::findOrFail($id);
        $property->update($request->except('landlord_id', 'type_id'));

        $type_id = $request->type_id;
        // $property->load('tenants');
        if($property->type_id != $type_id && $type_id == 3 && count($property->tenants()->get()) > 1 ){
            // if tenants are more than one, we do not allow this
            return response()->json(['message' => 'This type of property does not allow more than one user.'], 400);
        }
        $property->update(['type_id' => $request->type_id]);

        $landlord_id = $request->has('landlord_id') ? $request['landlord_id'] : null;

        if(isset($landlord_id)){
            $currentLandlordEntry = $property->landlord()->wherePivot('end_date', null)->first();
            if(isset($currentLandlordEntry)){
                $property->landlord()->wherePivot('end_date', null)->updateExistingPivot($currentLandlordEntry->id, array(
                    'end_date' => now(),
                ));
            }
            $newLandlord = User::findOrFail($landlord_id);
            $start_date = now();
            $end_date = null;
            $property->landlord()->save($newLandlord, array('start_date' => $start_date, 'end_date' => $end_date));
        }
 
        $lastPaymentForThisProperty = $property->payments()->orderByDesc('end_date')->first();
        if(isset($lastPaymentForThisProperty)){
            $property['last_payment_date'] = $lastPaymentForThisProperty->end_date;
            if(new DateTime($lastPaymentForThisProperty->end_date) < new DateTime()){
                $property['payment_status'] = "behind";
            }
            else{
                $property['payment_status'] = "clear";
            }
        }
        else {
            $property['last_payment_date'] = (new DateTime($property->created_at))->modify('last day of this month')->format("Y-m-d");
            $property['payment_status'] = "new";
        }

        return $property->load('tenants', 'landlord', 'occupants', 'type');
    }

    public function destroy(Request $request, $id)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }
        Property::destroy($id);
        return response()->json(['message'=>'Property successfully deleted.'], 200);
    }
}
