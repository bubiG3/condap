<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class UserRoleController extends Controller
{
    public function index()
    {
        // if (! Gate::allows('isAdmin')) {
        //     return response()->json(['message'=>'Unauthorized.'], 403);
        // }
        return UserRole::all();
    }
 
    public function show($id)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }
        return UserRole::find($id);
    }

    public function store(Request $request)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }
        return UserRole::create($request->all());
    }

    public function update(Request $request, $id)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }
        $userRole = UserRole::findOrFail($id);
        $userRole->update($request->all());

        return $userRole;
    }

    public function destroy(Request $request, $id)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json(['message'=>'Unauthorized.'], 403);
        }
        $userRole = UserRole::findOrFail($id);
        $userRole->destroy();

        return 204;
    }
}
