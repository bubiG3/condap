<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feedback;

class FeedbackController extends Controller
{
    public function index()
    {
        $feedbacks = Feedback::with('user')->get();
        return response()->json($feedbacks, 200);
    }
 
    public function show($id)
    {
        return response()->json(Feedback::with('user')->find($id));
    }

    public function store(Request $request)
    {
        $feedback = Feedback::create($request->all());
        $feedback = Feedback::with('user')->findOrFail($feedback->id);
        return $feedback;
    }

    public function update(Request $request, $id)
    {
        $feedback = Feedback::findOrFail($id);
        $feedback->update($request->all());
        $feedback = Feedback::with('user')->findOrFail($id);
        return response()->json($feedback);
    }

    public function destroy(Request $request, $id)
    {
        Feedback::destroy($id);
        return response()->json(['message'=>'Feedback successfully deleted.'], 200);
    }
}
