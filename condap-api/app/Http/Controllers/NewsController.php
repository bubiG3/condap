<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function index()
    {
        return News::all()->reverse()->values();
    }
 
    public function show($id)
    {
        return News::find($id);
    }

    public function store(Request $request)
    {
        $pictureName = $this->getFileNameAndSavePic($request, 'default_news.png');
        $updatedFields = $request->except('picture');
        $updatedFields['picture'] = $pictureName;
        return News::create($updatedFields);
    }

    public function update(Request $request, $id)
    {
        $news = News::findOrFail($id);
        $news->update($request->except('picture'));
        return $news;
    }

    public function updateNewsPicture(Request $request, $id)
    {
        $news = News::findOrFail($id);
        $updatedPicture = $this->getFileNameAndSavePic($request, $news->picture);
        $news->update(['picture'=>$updatedPicture]);
        return News::findOrFail($id);
    }

    public function destroy(Request $request, $id)
    {
        $news = News::findOrFail($id);
        $news->destroy();

        return 204;
    }

    // HELPER

    public function getFileNameAndSavePic($request, $oldPictureName){
        if($request->hasFile('picture')){
            $validatedPicture = $request->validate([
                'picture' => 'mimes:jpg,jpeg,png,bmp,tiff|max:10000'
            ]);

            if(isset($oldPictureName)) {
                if($oldPictureName!='default_news.png'){
                    Storage::delete('public/news/'.$oldPictureName);
                }
            }
            // set a random name to the picture, casting it to a string
            $fileName = (string) Str::uuid();
            $originalFileExtension = $request->file('picture')->getClientOriginalExtension();
            $fullFileName = $fileName.'.'.$originalFileExtension;
            // and save it to storage
            $path = $request->file('picture')->storeAs('public/news', $fullFileName);
            return $fullFileName;
        }
        // else set filename to that of the default pic
        return $request->isMethod('patch') ? abort(406, 'A file must be attached.') : 'default_news.png';
    }
}
