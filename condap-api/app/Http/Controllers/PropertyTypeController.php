<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PropertyType;

class PropertyTypeController extends Controller
{
    public function index()
    {
        return PropertyType::all();
    }
 
    public function show($id)
    {
        return PropertyType::find($id);
    }

    public function store(Request $request)
    {
        return PropertyType::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $propertyType = PropertyType::findOrFail($id);
        $propertyType->update($request->all());

        return $propertyType;
    }

    public function destroy(Request $request, $id)
    {
        $propertyType = PropertyType::findOrFail($id);
        $propertyType->destroy();

        return 204;
    }
}
