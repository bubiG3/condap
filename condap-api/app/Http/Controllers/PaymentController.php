<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Carbon;

use App\Models\Payment;
use App\Models\Image;
use App\Models\Receipt;
use App\Models\User;
use App\Models\Property;

use PDF;
use DateTime;
use DateInterval;
use DatePeriod;

class PaymentController extends Controller
{
    /**
     * Admin get all payments. Q: Should the receipt also be attached if present?
     * Tenants and occupants see ONLY their own payments.
     * TODO: Landlord sees all for their props
     */
    public function index()
    {
        if (! Gate::allows('isAdmin')) {
            if(Gate::allows('isLandlord')){
                global $ids;
                $ids = [];
                Auth::user()->properties()->each(function ($property) {
                    array_push($GLOBALS['ids'], $property->id);
                });
                return Payment::whereIn('property_id' , $ids)->with('property', 'user', 'status')->get()->load('pictures')->reverse()->values();
            }
            return Payment::where('user_id', Auth::id())->with('property', 'user', 'status')->get()->load('pictures')->reverse()->values();
        }
        return Payment::with('property', 'user', 'status')->get()->load('pictures')->reverse()->values();
    }
 
    /**
     * TODO ?:
     * Admin get all properties, with all their info attached.
     * Landlord gets all their proeprties asociated to them.
     * Tenants & occupants can see only their property.
     */
    public function show($id)
    {
        return Payment::with('property', 'user', 'status')->find($id)->load('pictures');
    }

    public function store(Request $request)
    {
        $paymentWithoutAttatchment = $request->except('pictures');
        
        $paymentWithoutAttatchment['status_id'] = '1';
        $paymentWithoutAttatchment['user_id'] = Auth::id();
        
        $payment = Payment::create($paymentWithoutAttatchment);
        $this->savePaymentFiles($request, $payment);
        return Payment::with('property', 'user', 'status')->findOrFail($payment->id)->load('pictures');
    }

    public function destroy(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);
        //delete the files as well!!
        $payment->destroy();

        return response()->json(['message'=>'Payment successfully deleted.'], 200);
    }

    //TODO: Update CURRENT proof of payment when rejected
    public function updateAfterRejected(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);
        if(!isset($payment) || $payment->status_id != 3){ //we allow update only when payment has been rejected
            return response()->json(['message'=>'Operation forbidden.'], 403);
        }

        $paymentWithoutAttatchment = $request->except('pictures');
        $pictureName = $this->savePaymentFiles($request, $payment);
        $paymentWithoutAttatchment['status_id'] = '1';
        $payment->update($paymentWithoutAttatchment);
        return Payment::with('property', 'user', 'status')->findOrFail($payment->id)->load('pictures');
    }
    // SPECIAL

    public function approvePaymentAndCreateReceipt($id)
    {
        $pictureName = $this->generateReceiptAndGetReceiptName($id);
        $receipt = Receipt::create(['payment_id'=>$id, 'picture'=>$pictureName]);
        $payment = Payment::findOrFail($id);
        $payment->update([
            'status_id' => '2'
        ]);
        return Payment::with('property', 'user', 'status')->findOrFail($id)->load('pictures');
    }

    public function denyPayment(Request $request, $id)
    {
        // dd($request->message);
        $payment = Payment::findOrFail($id);
        $payment->update([
            'status_id' => '3',
            'message' => $request->message
        ]);
        return Payment::with('property', 'user', 'status')->findOrFail($id);
    }

    // HELPER

    public function savePaymentFiles($request, $payment){
        if($request->has('pictures')){
            $allowedfileExtension=['pdf', 'jpg', 'jpeg', 'png'];
            $files = $request->file('pictures');
            $newFilesIds = [];

            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension,$allowedfileExtension);
                if($check) {
                    // set a random name to the picture, casting it to a string
                    $fileName = (string) Str::uuid();
                    $originalFileExtension = $file->getClientOriginalExtension();
                    $fullFileName = $fileName.'.'.$originalFileExtension;
                    // create Image
                    $image = Image::create(['payment_id'=>$payment->id, 'name'=>$fullFileName]);
                    array_push($newFilesIds, $image->id);
                    // and save it to storage
                    $file->storeAs('public/proof_of_payment', $fullFileName);
                    //$file->storeAs('public/norms', $fileName);

                }
                else {
                    return abort(406, 'File(s) not in correct format.');
                }
            }
            // chech if payment was perviously attached with documents and delete the old ones
            $allPaymentFiles = $payment->load('pictures')->pictures;
            foreach ($allPaymentFiles as $oldFile) {
                if(!in_array($oldFile->id, $newFilesIds)){
                    Storage::delete('public/proof_of_payment/'.$oldFile->name);
                    Image::destroy($oldFile->id);
                }
            }
            return true;
        }
        return abort(406, 'A file must be attached.');
    }

    public function generateReceiptAndGetReceiptName($id){
        $acceptedPayment = Payment::findOrFail($id);

        if(isset($acceptedPayment)){
            $property = Property::findOrFail($acceptedPayment->property_id);
            $user = User::findOrFail($acceptedPayment->user_id);
            $admin = Auth::user();
            $taxPerMonth = $this->determineTaxPerMonth($acceptedPayment);
            $paymentPeriod = $this->determinePeriod($acceptedPayment);
            $amountPaid = $taxPerMonth * count($paymentPeriod);
            $laggingMonthsCount = count($this->determineLagPeriod($property));
            $balanceBeforePayment = $taxPerMonth * $laggingMonthsCount;

            $receiptData = [
                'currentDate' => strval(Carbon::now()->format('Y-m-d')),
                'propertyParcelNumber' => strval($property->parcel_number),
                'propertyBlock' => $property->block,
                'amountPaid' => strval($amountPaid),
                'taxPerMonth' => $taxPerMonth,
                'paymentPeriod' => $paymentPeriod,
                // 'bankReference' => ,
                'userFullName' => $user->first_name .' '. $user->last_name,
                'adminFullName' => $admin->first_name .' '. $admin->last_name,
                'startDate' => $acceptedPayment->start_date,
                'endDate' => $acceptedPayment->end_date,
                'balanceBeforePayment' => $balanceBeforePayment,
                'balanceAfterPayment' => $amountPaid - $balanceBeforePayment,
            ];
            $html = htmlentities(view('receipt', ['receiptData'=>$receiptData]));            
            $pdf = PDF::loadHTML(html_entity_decode($html, ENT_HTML5));
            // Generate name
            $fileName = (string) Str::uuid();
            $fullFileName = $fileName.'.pdf';
            // Save file to storage
            Storage::put('public/receipts/'.$fullFileName, $pdf->download());

            return $fullFileName;
        }

        return abort();
    }

    /**
     * Determine tax based on type of property and occupance.
     * Tax = 35 if the property type is appartment or land OR
     * if the property is unoccupied and of type uni-/multy-house.
     * In all other cases tax = 50.
     */
    public function determineTaxPerMonth($payment) {
        $property = Property::find($payment->property_id);
        return
        ($property->type_id == 1 || $property->type_id == 2) ||
        (($property->type_id == 3 || $property->type_id == 4) && (count($property->tenants()->get()) <= 0 || count($property->occupants()->get()) <= 0))
        ? 35 : 50;
    }

    /**
     * The whole payment period.
     */
    public function determinePeriod($payment) {
        $startMonth = (new DateTime($payment->start_date))->modify('first day of this month');
        $endMonth = (new DateTime($payment->end_date))->modify('last day of this month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($startMonth, $interval, $endMonth);
        $periodArray = [];
        foreach ($period as $dt) {
            array_push($periodArray, $dt->format("M Y"));
        }
        return $periodArray;
    }

    /**
     * Period of "months behind". Checks how many months the user had to pay for prior to placing the payment.
     */
    public function determineLagPeriod($property) {
        $lastApprovedPayment = $property->payments()->orderByDesc('end_date')->first();
        $periodArray = [];
        if(isset($lastApprovedPayment)){
            $startMonth = (new DateTime($lastApprovedPayment->end_date))->modify('last day of this month');
            $endMonth = (new DateTime())->modify('last day of last month');
            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($startMonth, $interval, $endMonth);
            foreach ($period as $dt) {
                array_push($periodArray, $dt->format("M Y"));
            }
        }
        return $periodArray; 
    }
}
