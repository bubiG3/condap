<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Receipt;

class FileController extends Controller
{
    public function profilePicture($fileName){
        $path = Storage::disk('public')->path('profile_pictures');
        $url = url('storage/profile_pictures/'.$fileName);
        $images = Storage::disk('public')->path('profile_pictures/'.$fileName);
        return response()->json([
            'images' => $url,
        ], 200);
    }

    public function news($fileName){      
        $path = Storage::disk('public')->path('news');
        $url = url('storage/news/'.$fileName);
        $images = Storage::disk('public')->path('news/'.$fileName);
        return response()->json([
            'images' => $url,
        ], 200);
    }

    public function payment($fileName){
        $path = Storage::disk('public')->path('proof_of_payment');
        $url = url('storage/proof_of_payment/'.$fileName);
        $images = Storage::disk('public')->path('proof_of_payment/'.$fileName);
        return response()->json([
            'images' => $url,
        ], 200);
    }

    public function receipt($paymentId){
        $payment = Payment::findOrFail($paymentId);
        $receipt = Receipt::where('payment_id', $payment->id)->first();
        $fileName = $receipt->picture;

        $path = Storage::disk('public')->path('receipts');
        $url = url('storage/receipts/'.$fileName);
        $images = Storage::disk('public')->path('receipts/'.$fileName);
        return response()->json([
            'images' => $url,
        ], 200);
    }

    public function norm($fileName){
        $url = url('storage/norms/'.$fileName);
        return response()->json([
            'images' => $url,
        ], 200);
    }
}
