<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Receipt;

class ReceiptController extends Controller
{
    public function index()
    {
        return Receipt::with('payment')->get();
    }
 
    public function show($id) // ТОDО: pass id of payment 
    {
        return Receipt::with('payment')->find($id);
    }

    public function update(Request $request, $id)
    {
        $receipt = Receipt::findOrFail($id);
        $receipt->update($request->all());
        return Receipt::with('payment')->find($receipt->id);
    }

    public function destroy(Request $request, $id)
    {
        $receipt = Receipt::findOrFail($id);
        $receipt->destroy();

        return 204;
    }
}
