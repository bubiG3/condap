<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PaymentStatusController extends Controller
{
    public function index()
    {
        return PaymentStatus::all();
    }
 
    public function show($id)
    {
        return PaymentStatus::find($id);
    }

    public function store(Request $request)
    {
        return PaymentStatus::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $payment = PaymentStatus::findOrFail($id);
        $payment->update($request->all());

        return $payment;
    }

    public function destroy(Request $request, $id)
    {
        if (! Gate::allows('isAdmin')) {
            return response()->json([
                'message' => 'Unauthorized.'
            ], 403);
        }
        $payment = PaymentStatus::findOrFail($id);
        $payment->destroy();

        return response()->json(['message'=>'Payment status successfully deleted.'], 200);    }
}
