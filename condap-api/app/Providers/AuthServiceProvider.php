<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
   
        Gate::define('isAdmin', function($user) {
           return $user->role_id === 1;
        });
       
        Gate::define('isLandlord', function($user) {
            return $user->role_id === 2;
        });

        Gate::define('isTenantOrOccupant', function($user) {
            return $user->role_id === 3 || $user->role_id === 4;
        });

        Gate::define('canUpdate', function($authUser, $userToUpdate) {
            if($authUser->id === $userToUpdate->id) {
                return true;
            }
            if($authUser->role_id === 1) {
                return true;
            }
            return false;
        });
    }
}
