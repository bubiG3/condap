<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\Notification;

class TestEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $notification;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Notification $param)
    {
        $this->notification = $param;
    }

    // TODO: For now we are sending the whole notification. Later we should hide some stuff.
    public function broadcastWith(){
        return [$this->notification];
    }

    /**
     * Broadcast messages to all channels that have been selected by the admin.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $roleIdsToBroadcastTo = json_decode($this->notification->target_roles);
        $arrayOfRoleChannels = [];
        foreach ($roleIdsToBroadcastTo as $userRoleId) {
            array_push($arrayOfRoleChannels, new Channel($userRoleId));
        }
        return $arrayOfRoleChannels;
    }
}
